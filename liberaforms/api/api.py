"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from liberaforms.utils import jwt_auth
from flask import current_app, Blueprint, jsonify
from liberaforms.models.site import Site
from liberaforms.models.schemas.site import SiteSchema
from liberaforms.models.schemas.form import FormSchema
from liberaforms.models.schemas.answer import AnswerSchema
from liberaforms.utils import utils
from liberaforms.utils import html_parser

api_bp = Blueprint('api_bp', __name__)


"""Unsensitve site information only."""
@api_bp.route('/api/site/info', methods=['GET'])
def site_info():
    site = SiteSchema(only=['created', 'hostname']).dump(Site.find())
    site['version'] = site.app_version()
    site['timezone'] = current_app.config['DEFAULT_TIMEZONE']
    return jsonify(site=site), 200


"""Public available form information only."""
@api_bp.route('/api/form/<int:form_id>', methods=['GET'])
@jwt_auth.instantiate_form
def form_info(form_id, **kwargs):
    form = kwargs["form"]
    if form.id != form_id:
        return jsonify({'message': 'Not acceptable'}), 406
    if form.is_public():
        return jsonify(form=FormSchema(only=['slug',
                                             'created',
                                             'introduction_md',
                                             'structure']).dump(form)), 200
    return jsonify({'message': 'Denied'}), 401


@api_bp.route('/api/form/<int:form_id>/answers', methods=['GET'])
@jwt_auth.instantiate_form
def form_answers(form_id, **kwargs):
    form = kwargs["form"]
    if form.id != form_id:
        return jsonify({'message': 'Not acceptable'}), 406
    return jsonify(answers=AnswerSchema(many=True).dump(form.answers),
                   meta={}), 200
