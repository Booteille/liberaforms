"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from flask import Blueprint, request, jsonify
from liberaforms.utils import auth
from liberaforms.utils import validators

form_style_bp = Blueprint('form_style_bp', __name__,
                          template_folder='../templates/form')


@form_style_bp.route('/form/<int:form_id>/set-font-color', methods=['POST'])
@auth.enabled_editor_required__json
@auth.instantiate_form(allow_admin=False)
def set_font_combo(form_id, **kwargs):
    form = kwargs['form']
    if 'foreground_color' in request.form and 'background_color' in request.form:
        if request.form['foreground_color'] and request.form['background_color']:
            # TODO: check for valid HTML color codes
            combo = {"foreground": request.form['foreground_color'],
                     "background": request.form['background_color']}
            form.style["colors"] = combo
            form.save()
            return jsonify(combo=form.style["colors"]), 200
    return jsonify("Not Acceptable"), 406


@form_style_bp.route('/form/<int:form_id>/set-background-color', methods=['POST'])
@auth.enabled_editor_required__json
@auth.instantiate_form(allow_admin=False)
def set_background_color(form_id, **kwargs):
    form = kwargs['form']
    if 'background_color' in request.form and request.form['background_color']:
        # TODO: check for valid HTML color codes
        color = request.form['background_color']
        form.style["background_color"] = color
        form.style["background_img"] = ""
        form.save()
        return jsonify(background_color=form.style["background_color"],
                       background_image=form.style["background_img"]), 200
    return jsonify("Not Acceptable"), 406


@form_style_bp.route('/form/<int:form_id>/set-background-image', methods=['POST'])
@auth.enabled_editor_required__json
@auth.instantiate_form(allow_admin=False)
def set_background_image(form_id, **kwargs):
    form = kwargs['form']
    if 'image_url' in request.form and request.form['image_url']:
        image_url = request.form['image_url']
        if not validators.is_valid_image_url(image_url):
            return jsonify("Not Acceptable"), 406
        form.style["background_img"] = image_url
        form.style["background_color"] = ""
        form.save()
        return jsonify(background_color=form.style["background_color"],
                       background_image=form.style["background_img"]), 200
    return jsonify("Not Acceptable"), 406


@form_style_bp.route('/form/<int:form_id>/set-header-image', methods=['POST'])
@auth.enabled_editor_required__json
@auth.instantiate_form(allow_admin=False)
def set_header_image(form_id, **kwargs):
    form = kwargs['form']
    if 'image_url' in request.form and request.form['image_url']:
        image_url = request.form['image_url']
        if not validators.is_valid_image_url(image_url):
            return jsonify("Not Acceptable"), 406
        form.style["header_img"] = image_url
        form.thumbnail = form.style["header_img"]
        form.save()
        return jsonify(image_url=form.style["header_img"]), 200
    return jsonify("Not Acceptable"), 406


@form_style_bp.route('/form/<int:form_id>/clear-style', methods=['POST'])
@auth.enabled_editor_required__json
@auth.instantiate_form(allow_admin=False)
def clear_all(form_id, **kwargs):
    form = kwargs['form']
    form.style = {
        "colors": {},
        "header_img": "",
        "background_img": "",
        "background_color": "",
    }
    form.thumbnail = None
    form.save()
    return jsonify("ok"), 200
