"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import datetime
from sqlalchemy.orm.attributes import flag_modified
from flask import current_app, Blueprint, request, redirect, jsonify, url_for
from flask_babel import gettext as _
import flask_login
from liberaforms.models.form import Form
from liberaforms.models.user import User
from liberaforms.models.formuser import FormUser
from liberaforms.models.answer import Answer
from liberaforms.models.schemas.form import FormSchemaForMyFormsDataDisplay, \
                                            FormSchemaForAdminFormsDataDisplay
from liberaforms.models.schemas.user import UserSchemaForAdminDataDisplay
from liberaforms.models.schemas.answer import AnswerSchema
from liberaforms.utils.utils import human_readable_bytes
from liberaforms.utils import validators
from liberaforms.utils import auth

from liberaforms import csrf

#from pprint import pprint

data_display_bp = Blueprint('data_display_bp', __name__)


# ## Admin Forms list

default_admin_forms_field_index = [
                {'name': 'form_name__html', 'label': 'Slug'},
                {'name': 'total_answers', 'label': 'Answers'},
                {'name': 'last_answer_date', 'label': 'Last answer'},
                {'name': 'author__html', 'label': 'Author'},
                {'name': 'total_users', 'label': 'Users'},
                {'name': 'is_public', 'label': 'Public'},
                {'name': 'created', 'label': 'Created'}
            ]


def get_admin_forms_field_index(user):
    if 'field_index' in user.admin['forms']:
        return user.admin['forms']['field_index']
    return default_admin_forms_field_index


def get_admin_forms_ascending(user):
    if 'ascending' in user.admin['forms']:
        return user.admin['forms']['ascending']
    return True


def get_admin_forms_order_by(user):
    if 'order_by' in user.admin['forms']:
        return user.admin['forms']['order_by']
    return 'created'


@data_display_bp.route('/data-display/admin/forms', methods=['GET'])
@auth.enabled_admin_required__json
def admin_forms():
    """ Returns json required by Vue dataDisplay component.
    """
    forms = Form.find_all()
    items = []
    for form in FormSchemaForAdminFormsDataDisplay(many=True).dump(forms):
        item = {}
        data = {}
        data['form_name__html'] = {
            'value': form['slug'],
            'html': f"<a href='{url_for('admin_bp.inspect_form', form_id=form['id'])}'>{form['slug']}</a>"
        }
        avatar_src = form['author']['avatar_src']
        avatar = f"<span class='ds-avatar'><img src='{avatar_src}' alt='' class='rounded-circle'/></span>"
        data['author__html'] = {
            'value': form['author']['name'],
            'html': f"<a href='{url_for('admin_bp.inspect_user', user_id=form['author']['id'])}'>{avatar}{form['author']['name']}</a>"
        }
        for field_name in form.keys():
            if field_name == 'slug':
                continue
            if field_name == 'id':
                item[field_name] = form[field_name]
                continue
            if field_name == 'created':
                item[field_name] = form[field_name]
                continue
            data[field_name] = form[field_name]
        item['data'] = data
        item['title'] = form['slug']
        items.append(item)
    return jsonify(
        items=items,
        meta={
            'name': 'All forms',
            'data_type': 'form',
            'deleted_fields': [],
            'default_field_index': default_admin_forms_field_index,
            'editable_fields': False,
            'item_endpoint': None,
            'can_edit': False
        },
        user_prefs={
            'field_index': get_admin_forms_field_index(flask_login.current_user),
            'order_by': get_admin_forms_order_by(flask_login.current_user),
            'ascending': get_admin_forms_ascending(flask_login.current_user)}
    ), 200


@data_display_bp.route('/data-display/admin/forms/change-index', methods=['POST'])
@auth.enabled_admin_required__json
def admin_forms_change_field_index():
    """Change Admin's forms field index preference."""
    data = request.get_json(silent=True)
    if 'field_index' not in data:
        return jsonify("Not Acceptable"), 406
    field_index = data['field_index']
    if validators.compare_field_index(field_index, default_admin_forms_field_index):
        flask_login.current_user.admin['forms']['field_index'] = field_index
        flag_modified(flask_login.current_user, 'admin')
        flask_login.current_user.save()
    else:
        field_index = default_admin_forms_field_index
    return jsonify(field_index=field_index), 200


@data_display_bp.route('/data-display/admin/forms/reset-index', methods=['POST'])
@auth.enabled_admin_required__json
def admin_reset_forms_field_index():
    """Reset Admin's Form field index preference."""
    flask_login.current_user.admin['forms']['field_index'] = default_admin_forms_field_index
    flag_modified(flask_login.current_user, 'admin')
    flask_login.current_user.save()
    return jsonify(field_index=flask_login.current_user.admin['forms']['field_index']), 200


@data_display_bp.route('/data-display/admin/forms/order-by-field', methods=['POST'])
@auth.enabled_admin_required__json
def order_admin_forms_by_field():
    """Set Admin's forms order_by preference."""
    data = request.get_json(silent=True)
    if 'order_by_field_name' not in data:
        return jsonify("Not Acceptable"), 406
    field_names = [field['name'] for field in default_admin_forms_field_index]
    if data['order_by_field_name'] not in field_names:
        return jsonify("Not Acceptable"), 406
    flask_login.current_user.admin['forms']['order_by'] = data['order_by_field_name']
    flag_modified(flask_login.current_user, 'admin')
    flask_login.current_user.save()
    return jsonify(order_by_field_name=flask_login.current_user.admin['forms']['order_by']), 200


@data_display_bp.route('/data-display/admin/forms/toggle-ascending', methods=['POST'])
@auth.enabled_admin_required__json
def admin_forms_toggle_ascending():
    """Toggle Admin's forms ascending order preference."""
    preference = get_admin_forms_ascending(flask_login.current_user)
    flask_login.current_user.admin['forms']['ascending'] = not preference
    flag_modified(flask_login.current_user, 'admin')
    flask_login.current_user.save()
    return jsonify(ascending=flask_login.current_user.admin['forms']['ascending']), 200


# ## Admin Users list

default_admin_users_field_index = [
                {'name': 'username__html', 'label': 'User name'},
                {'name': 'total_forms', 'label': 'Forms'},
                {'name': 'email', 'label': 'Email'},
                {'name': 'enabled', 'label': 'Enabled'},
                {'name': 'role', 'label': 'Role'},
                {'name': 'disk_usage__html', 'label': 'Disk usage'},
                {'name': 'created', 'label': 'Created'}
            ]


def get_admin_users_field_index(user):
    if 'field_index' in user.admin['users']:
        return user.admin['users']['field_index']
    return default_admin_users_field_index


def get_admin_users_ascending(user):
    if 'ascending' in user.admin['users']:
        return user.admin['users']['ascending']
    return True


def get_admin_users_order_by(user):
    if 'order_by' in user.admin['users']:
        return user.admin['users']['order_by']
    return 'created'


@data_display_bp.route('/data-display/admin/users', methods=['GET'])
@auth.enabled_admin_required__json
def admin_users():
    """Return json required by Vue dataDisplay component."""
    users = User.find_all()
    items = []
    for user in UserSchemaForAdminDataDisplay(many=True).dump(users):
        item = {}
        data = {}
        avatar_src = user['avatar_src']
        avatar = f"<span class='ds-avatar'><img src='{avatar_src}' alt='' class='rounded-circle' /></span>"
        data['username__html'] = {
            'value': user['username'],
            'html': f"<a href='{url_for('admin_bp.inspect_user', user_id=user['id'])}'>{avatar}{user['username']}</a>"
        }
        data['disk_usage__html'] = {
            'value': user['disk_usage'],
            'html': human_readable_bytes(user['disk_usage'])
        }
        for field_name in user.keys():
            if field_name in ('username', 'disk_usage'):
                continue
            if field_name in ('id', 'created'):
                item[field_name] = user[field_name]
                continue
            data[field_name] = user[field_name]
        item['data'] = data
        item['title'] = user['username']
        items.append(item)
    return jsonify(
        items=items,
        meta={
            'name': 'Users',
            'data_type': 'user',
            'deleted_fields': [],
            'default_field_index': default_admin_users_field_index,
            'editable_fields': False,
            'item_endpoint': None,
            'can_edit': False,
            'enabled_exports': ['csv']
        },
        user_prefs={
            'field_index': get_admin_users_field_index(flask_login.current_user),
            'order_by': get_admin_users_order_by(flask_login.current_user),
            'ascending': get_admin_users_ascending(flask_login.current_user)}
    ), 200


@data_display_bp.route('/data-display/admin/users/change-index', methods=['POST'])
@auth.enabled_admin_required__json
def users_change_field_index():
    """Change User's forms field index preference."""
    data = request.get_json(silent=True)
    if 'field_index' not in data:
        return jsonify("Not Acceptable"), 406
    field_index = data['field_index']
    if validators.compare_field_index(field_index, default_admin_users_field_index):
        flask_login.current_user.admin['users']['field_index'] = field_index
        flag_modified(flask_login.current_user, 'admin')
        flask_login.current_user.save()
    else:
        field_index = default_admin_users_field_index
    return jsonify(field_index=field_index), 200


@data_display_bp.route('/data-display/admin/users/reset-index', methods=['POST'])
@auth.enabled_admin_required__json
def users_reset_field_index():
    """Reset User's field index preference."""
    flask_login.current_user.admin['users']['field_index'] = default_admin_users_field_index
    flag_modified(flask_login.current_user, 'admin')
    flask_login.current_user.save()
    return jsonify(field_index=flask_login.current_user.admin['users']['field_index']), 200


@data_display_bp.route('/data-display/admin/users/order-by-field', methods=['POST'])
@auth.enabled_admin_required__json
def users_order_by_field():
    """Set User's forms order_by preference."""
    data = request.get_json(silent=True)
    if 'order_by_field_name' not in data:
        return jsonify("Not Acceptable"), 406
    field_names = [field['name'] for field in default_admin_users_field_index]
    if not data['order_by_field_name'] in field_names:
        return jsonify("Not Acceptable"), 406
    flask_login.current_user.admin['users']['order_by'] = data['order_by_field_name']
    flag_modified(flask_login.current_user, 'admin')
    flask_login.current_user.save()
    return jsonify(order_by_field_name=flask_login.current_user.admin['users']['order_by']), 200


@data_display_bp.route('/data-display/admin/users/toggle-ascending', methods=['POST'])
@auth.enabled_admin_required__json
def users_toggle_ascending():
    """Toggle User's forms ascending order preference."""
    preference = get_admin_users_ascending(flask_login.current_user)
    flask_login.current_user.admin['users']['ascending'] = False if preference else True
    flag_modified(flask_login.current_user, 'admin')
    flask_login.current_user.save()
    return jsonify(ascending=flask_login.current_user.admin['users']['ascending']), 200


# ## Admin User's forms list

default_admin_userforms_field_index = [
                {'name': 'form_name__html', 'label': 'Slug'},
                {'name': 'total_answers', 'label': 'Answers'},
                {'name': 'last_answer_date', 'label': 'Last answer'},
                {'name': 'is_author', 'label': 'Author'},
                {'name': 'total_users', 'label': 'Users'},
                {'name': 'is_public', 'label': 'Public'},
                {'name': 'created', 'label': 'Created'},
            ]


def get_admin_userforms_field_index(user):
    if 'field_index' in user.admin['userforms']:
        return user.admin['userforms']['field_index']
    return default_admin_userforms_field_index


def get_admin_userforms_ascending(user):
    if 'ascending' in user.admin['userforms']:
        return user.admin['userforms']['ascending']
    return True


def get_admin_userforms_order_by(user):
    if 'order_by' in user.admin['userforms']:
        return user.admin['userforms']['order_by']
    return 'created'


@data_display_bp.route('/data-display/admin/user/<int:user_id>/forms', methods=['GET'])
@auth.enabled_admin_required__json
def admin_userforms(user_id):
    """Returns json required by Vue dataDisplay component."""
    user = User.find(id=user_id)
    forms = user.get_forms()
    items = []
    for form in FormSchemaForAdminFormsDataDisplay(many=True).dump(forms):
        item = {}
        data = {}
        slug = form['slug']
        data['form_name__html'] = {
            'value': slug,
            'html': f"<a href='{url_for('admin_bp.inspect_form', form_id=form['id'])}'>{form['slug']}</a>"
        }
        data['is_author'] = form['author_id'] == user.id
        for field_name in form.keys():
            if field_name == 'slug':
                continue
            if field_name == 'id':
                item[field_name] = form[field_name]
                continue
            if field_name == 'created':
                item[field_name] = form[field_name]
                continue
            data[field_name] = form[field_name]
        item['data'] = data
        item['title'] = form['slug']
        items.append(item)
    return jsonify(
        items=items,
        meta={
            'name': 'User forms',
            'data_type': 'form',
            'deleted_fields': [],
            'default_field_index': default_admin_userforms_field_index,
            'editable_fields': False,
            'item_endpoint': None,
            'can_edit': False,
        },
        user_prefs={
            'field_index': get_admin_userforms_field_index(flask_login.current_user),
            'order_by': get_admin_userforms_order_by(flask_login.current_user),
            'ascending': get_admin_userforms_ascending(flask_login.current_user)}
    ), 200


@data_display_bp.route('/data-display/admin/user/<int:user_id>/forms/change-index', methods=['POST'])
@auth.enabled_admin_required__json
def admin_userforms_change_field_index(user_id):
    """Change User's forms field index preference."""
    data = request.get_json(silent=True)
    if 'field_index' not in data:
        return jsonify("Not Acceptable"), 406
    field_index = data['field_index']
    if validators.compare_field_index(field_index, default_admin_userforms_field_index):
        flask_login.current_user.admin['userforms']['field_index'] = field_index
        flag_modified(flask_login.current_user, 'admin')
        flask_login.current_user.save()
    else:
        field_index = default_admin_userforms_field_index
    return jsonify(field_index=field_index), 200


@data_display_bp.route('/data-display/admin/user/<int:user_id>/forms/reset-index', methods=['POST'])
@auth.enabled_admin_required__json
def admin_userforms_reset_field_index(user_id):
    """Reset Users field index preference."""
    flask_login.current_user.admin['userforms']['field_index'] = default_admin_userforms_field_index
    flag_modified(flask_login.current_user, 'admin')
    flask_login.current_user.save()
    return jsonify(field_index=flask_login.current_user.admin['userforms']['field_index']), 200


@data_display_bp.route('/data-display/admin/user/<int:user_id>/forms/order-by-field', methods=['POST'])
@auth.enabled_admin_required__json
def admin_userforms_order_by_field(user_id):
    """Set User's forms order_by preference."""
    data = request.get_json(silent=True)
    if 'order_by_field_name' not in data:
        return jsonify("Not Acceptable"), 406
    field_names = [field['name'] for field in default_admin_userforms_field_index]
    if data['order_by_field_name'] not in field_names:
        return jsonify("Not Acceptable"), 406
    flask_login.current_user.admin['userforms']['order_by'] = data['order_by_field_name']
    flag_modified(flask_login.current_user, 'admin')
    flask_login.current_user.save()
    return jsonify(order_by_field_name=flask_login.current_user.admin['userforms']['order_by']), 200


@data_display_bp.route('/data-display/admin/user/<int:user_id>/forms/toggle-ascending', methods=['POST'])
@auth.enabled_admin_required__json
def admin_userforms_toggle_ascending(user_id):
    """Toggle User's forms ascending order preference."""
    preference = get_admin_userforms_ascending(flask_login.current_user)
    flask_login.current_user.admin['userforms']['ascending'] = not preference
    flag_modified(flask_login.current_user, 'admin')
    flask_login.current_user.save()
    return jsonify(ascending=flask_login.current_user.admin['userforms']['ascending']), 200


@data_display_bp.route('/data-display/admin/users/csv', methods=['GET'])
@auth.enabled_admin_required__json
def users_csv():
    """Redirect to the CSV download."""
    return redirect(url_for('admin_bp.csv_users'))


# ## My Forms

default_my_forms_field_index = [
                {'name': 'form_name__html', 'label': 'Name'},
                {'name': 'answers__html', 'label': 'Answers'},
                {'name': 'last_answer_date', 'label': 'Last answer'},
                {'name': 'is_public', 'label': 'Public'},
                {'name': 'is_shared', 'label': 'Shared'},
                {'name': 'created', 'label': 'Created'}
            ]


def get_my_forms_field_index(user):
    if 'forms_field_index' in user.preferences and user.preferences['forms_field_index']:
        return user.preferences['forms_field_index']
    return default_my_forms_field_index


def get_my_forms_ascending(user):
    if 'forms_ascending' in user.preferences and \
       isinstance(user.preferences['forms_ascending'], bool):
        return user.preferences['forms_ascending']
    return True


def get_my_forms_order_by(user):
    if 'forms_order_by' in user.preferences and user.preferences['forms_order_by']:
        return user.preferences['forms_order_by']
    return 'created'


@data_display_bp.route('/data-display/my-forms/<int:user_id>', methods=['GET'])
@csrf.exempt
@auth.enabled_user_required__json
def my_forms(user_id):
    """Return json required by Vue dataDisplay component.

    Hyperlinks to be rendered by the component and are declared by trailing '__html'
    """
    if not user_id == flask_login.current_user.id:
        return jsonify("Denied"), 401
    items = []
    #for form_user in FormUser.find_all(user_id=flask_login.current_user.id):
    for form_user in FormUser.find_all(user_id=user_id):
        form = FormSchemaForMyFormsDataDisplay().dump(form_user.form)
        item = {}
        data = {}
        item['id'] = form['id']
        item['created'] = form['created']
        data['last_answer_date'] = form['last_answer_date']
        form_url = form_user.get_landing_page()
        data['form_name__html'] = {
            'value': form['name'],
            'html': f"<a href='{form_url}'>{form['name']}</a>"
        }
        data['is_public'] = form['is_public']
        data['is_shared'] = form['is_shared']
        total_answers = form['total_answers']
        disk_usage = ""
        if form['attachments_usage'] is not None:
            bytes_str = human_readable_bytes(form['attachments_usage'])
            disk_usage = f"<span class='badge rounded-pill text-bg-light'>{bytes_str}</span>"
        data['answers__html'] = {
            'value': total_answers,
            'html': f"<span>{total_answers}</span> {disk_usage}"
        }
        item['data'] = data
        item['title'] = form['name']
        items.append(item)
    return jsonify(
        items=items,
        meta={
            'name': 'my-forms',
            'data_type': 'form',
            'deleted_fields': [],
            'default_field_index': default_my_forms_field_index,
            'editable_fields': False,
            'item_endpoint': None,
            'can_edit': False,
        },
        user_prefs={
            'field_index': get_my_forms_field_index(flask_login.current_user),
            'order_by': get_my_forms_order_by(flask_login.current_user),
            'ascending': get_my_forms_ascending(flask_login.current_user)
        }
    ), 200


@data_display_bp.route('/data-display/my-forms/<int:user_id>/change-index', methods=['POST'])
@auth.enabled_user_required__json
def change_forms_field_index(user_id):
    """Change Users' Form (all forms) field index preference."""
    if not user_id == flask_login.current_user.id:
        return jsonify("Forbidden"), 403
    data = request.get_json(silent=True)
    if 'field_index' not in data:
        return jsonify("Not Acceptable"), 406
    field_index = data['field_index']
    if validators.compare_field_index(field_index, default_my_forms_field_index):
        flask_login.current_user.preferences['forms_field_index'] = field_index
        flask_login.current_user.save()
    else:
        field_index = default_my_forms_field_index
    return jsonify(field_index=field_index), 200


@data_display_bp.route('/data-display/my-forms/<int:user_id>/reset-index', methods=['POST'])
@auth.enabled_user_required__json
def reset_forms_field_index(user_id):
    """Reset Users' Form field index preference."""
    if not user_id == flask_login.current_user.id:
        return jsonify("Forbidden"), 403
    flask_login.current_user.preferences['forms_field_index'] = default_my_forms_field_index
    flask_login.current_user.save()
    return jsonify(field_index=flask_login.current_user.preferences['forms_field_index']), 200


@data_display_bp.route('/data-display/my-forms/<int:user_id>/order-by-field', methods=['POST'])
@auth.enabled_user_required__json
def order_forms_by_field(user_id):
    """Set User's forms order_by preference."""
    if not user_id == flask_login.current_user.id:
        return jsonify("Forbidden"), 403
    data = request.get_json(silent=True)
    if 'order_by_field_name' not in data:
        return jsonify("Not Acceptable"), 406
    field_names = [field['name'] for field in default_my_forms_field_index]
    if not data['order_by_field_name'] in field_names:
        return jsonify("Not Acceptable"), 406
    flask_login.current_user.preferences['forms_order_by'] = data['order_by_field_name']
    flask_login.current_user.save()
    return jsonify(order_by_field_name=flask_login.current_user.preferences['forms_order_by']), 200


@data_display_bp.route('/data-display/my-forms/<int:user_id>/toggle-ascending', methods=['POST'])
@auth.enabled_user_required__json
def forms_toggle_ascending(user_id):
    """Toggle User's forms ascending order preference."""
    if not user_id == flask_login.current_user.id:
        return jsonify("Forbidden"), 403
    preference = get_my_forms_ascending(flask_login.current_user)
    flask_login.current_user.preferences['forms_ascending'] = not preference
    flask_login.current_user.save()
    return jsonify(ascending=flask_login.current_user.preferences['forms_ascending']), 200


# ## A my-form

@data_display_bp.route('/data-display/form/<int:form_id>', methods=['GET'])
@auth.enabled_user_required__json
@auth.instantiate_form__json(allow_admin=False, allow_guest=True)
def form_answers(form_id, **kwargs):
    """Return json required by vue data-display component."""
    form = kwargs["form"]
    form_user = kwargs["form_user"]
    default_field_index = form.get_field_index_for_display()
    field_index = form_user.get_field_index_preference()
    order_by = form_user.get_order_by_field()
    user_prefs = {
        'field_index': field_index if field_index else default_field_index,
        'order_by': order_by if order_by else 'created',
        'ascending': form_user.ascending,
        'pdf': form_user.ui_preferences["pdf"] if "pdf" in form_user.ui_preferences else None,
        'show_chrono_graph': "chrono_answers" in form_user.ui_preferences and \
                            form_user.ui_preferences["chrono_answers"],
        'extended_filters': form_user.ui_preferences["extended_filters"] if "extended_filters" in form_user.ui_preferences else {}
    }
    return jsonify(
        items=AnswerSchema(many=True).dump(form.answers),
        meta={
            'name': form.slug,
            'data_type': 'answer',
            'deleted_fields': form.get_deleted_fields(),
            'default_field_index': default_field_index,
            'form_structure': form.structure,
            'item_endpoint': '/data-display/answer/',
            'can_edit': form_user.is_editor,
            'show_modes': True,
            'enabled_exports': ['csv', 'json', 'pdf'],
            'title': form.name,
        },
        user_prefs=user_prefs
    ), 200


@data_display_bp.route('/data-display/form/<int:form_id>/change-index', methods=['POST'])
@auth.enabled_user_required__json
@auth.instantiate_form__json(allow_admin=False, allow_guest=True)
def change_answer_field_index(form_id, **kwargs):
    """Change User's Answer field index preference for this form."""
    form_user = kwargs["form_user"]
    data = request.get_json(silent=True)
    if 'field_index' not in data:
        return jsonify("Not Acceptable"), 406
    field_index = data['field_index']
    default_field_index = form_user.form.get_field_index_for_display()
    if validators.compare_field_index(field_index, default_field_index):
        form_user.save_field_index_preference(field_index)
        return jsonify(field_index=field_index), 200
    form_user.save_field_index_preference(default_field_index)
    return jsonify(field_index=default_field_index), 200


@data_display_bp.route('/data-display/form/<int:form_id>/reset-index', methods=['POST'])
@auth.enabled_user_required__json
@auth.instantiate_form__json(allow_admin=False, allow_guest=True)
def reset_answers_field_index(form_id, **kwargs):
    """Reset User's Answer field index preference for this form."""
    form_user = kwargs["form_user"]
    field_index = form_user.form.get_field_index_for_display()
    form_user.save_field_index_preference(field_index)
    return jsonify(field_index=field_index), 200


@data_display_bp.route('/data-display/form/<int:form_id>/order-by-field', methods=['POST'])
@auth.enabled_user_required__json
@auth.instantiate_form__json(allow_admin=False, allow_guest=True)
def order_answers_by_field(form_id, **kwargs):
    """Set User's order answers by preference for this form."""
    form_user = kwargs["form_user"]
    data = request.get_json(silent=True)
    if 'order_by_field_name' not in data:
        return jsonify("order_by_field_name required"), 406
    field_preference = data['order_by_field_name']
    field_names = [field['name'] for field in form_user.form.fieldIndex]
    if field_preference not in field_names:
        return jsonify("Not a valid field name"), 406
    form_user.save_order_by(field_preference)
    return jsonify(order_by_field_name=field_preference), 200


@data_display_bp.route('/data-display/form/<int:form_id>/toggle-ascending', methods=['POST'])
@auth.enabled_user_required__json
@auth.instantiate_form__json(allow_admin=False, allow_guest=True)
def answers_toggle_ascending(form_id, **kwargs):
    """Toggle user's ascending order preference for this Form's Answers."""
    form_user = kwargs["form_user"]
    return jsonify(ascending=form_user.toggle_ascending_order()), 200


@data_display_bp.route('/data-display/form/<int:form_id>/set-landing-page', methods=['PATCH'])
@auth.enabled_user_required__json
@auth.instantiate_form(allow_admin=False, allow_guest=True)
def set_landing_page(form_id, **kwargs):
    content = request.get_json()
    if 'page' in content and \
       content['page'] in FormUser.avaiable_landing_pages():
        form_user = kwargs['form_user']
        form_user.ui_preferences["answers_landing_page"] = content['page']
        flag_modified(form_user, "ui_preferences")
        form_user.save()
        return jsonify(msg=form_user.ui_preferences["answers_landing_page"]), 200
    return jsonify("Not Acceptable"), 406


@data_display_bp.route('/data-display/form/<int:form_id>/set-filters', methods=['PATCH'])
@auth.enabled_user_required__json
@auth.instantiate_form(allow_admin=False, allow_guest=True)
def set_extended_filters(form_id, **kwargs):
    content = request.get_json()
    if 'extended_filters' in content: # TODO. also check validity
        form_user = kwargs['form_user']
        form_user.ui_preferences["extended_filters"] = content['extended_filters']
        flag_modified(form_user, "ui_preferences")
        form_user.save()
        return jsonify(msg=form_user.ui_preferences["extended_filters"]), 200
    return jsonify("Not Acceptable"), 406


@data_display_bp.route('/data-display/form/<int:form_id>/set-pdf-preferences', methods=['PATCH'])
@auth.enabled_user_required__json
@auth.instantiate_form(allow_admin=False, allow_guest=True)
def set_pdf_prefs(form_id, **kwargs):
    content = request.get_json()
    if 'prefs' in content: # TODO. also check validity
        form_user = kwargs['form_user']
        form_user.ui_preferences["pdf"] = content['prefs']
        flag_modified(form_user, "ui_preferences")
        form_user.save()
        return jsonify(msg=form_user.ui_preferences["pdf"]), 200
    return jsonify("Not Acceptable"), 406


@data_display_bp.route('/data-display/form/<int:form_id>/toggle-chrono-graph', methods=['PATCH'])
@auth.enabled_user_required__json
@auth.instantiate_form(allow_admin=False, allow_guest=True)
def toggle_crono_graph(form_id, **kwargs):
    content = request.get_json()
    if 'visible' in content and isinstance(content['visible'], bool):
        print('is bool', content['visible'])
        form_user = kwargs['form_user']
        form_user.ui_preferences["chrono_answers"] = content['visible']
        flag_modified(form_user, "ui_preferences")
        form_user.save()
        return jsonify(visible=form_user.ui_preferences["chrono_answers"]), 200
    return jsonify("Not Acceptable"), 406


@data_display_bp.route('/data-display/form/<int:form_id>/delete-all-items', methods=['GET'])
@auth.enabled_editor_required__json
@auth.instantiate_form__json(allow_admin=False)
def delete_all_answers(form_id, **kwargs):
    """Redirect to the delete all answers page."""
    form = kwargs["form"]
    return redirect(url_for('answers_bp.delete_all', form_id=form.id))


# ## Answers

@data_display_bp.route('/data-display/answer/<int:answer_id>/mark', methods=['POST'])
@auth.enabled_user_required__json
@auth.instantiate_answer__json(allow_guest=True)
def toggle_answer_mark(answer_id, **kwargs):
    answer = kwargs['answer']
    answer.marked = not answer.marked
    answer.save()
    return jsonify(marked=answer.marked), 200


@data_display_bp.route('/data-display/answer/<int:answer_id>/save', methods=['POST', 'PATCH'])
@auth.enabled_editor_required__json
@auth.instantiate_answer__json()
def update_answer(answer_id, **kwargs):
    answer = kwargs['answer']
    content = request.get_json()
    try:
        if not isinstance(content['item_data'], dict):
            return jsonify("Not an array"), 406
        answer.save_previous_answer(flask_login.current_user.id)
        answer.previous_id = answer.editions[0].id
        answer.data = content['item_data']
        answer.updated = datetime.datetime.now(datetime.timezone.utc)
        answer.save()
        answer.form.expired = answer.form.has_expired()
        answer.form.save()
        answer.form.add_log(answer.get_history_link(link_text=_("Modified an answer")))
        return jsonify(saved=True, data=answer.data), 200
    except Exception as error:
        current_app.logger.warning(error)
        return jsonify(saved=False), 406


@data_display_bp.route('/data-display/answer/<int:answer_id>/delete', methods=['DELETE'])
@auth.enabled_editor_required__json
@auth.instantiate_answer__json()
def delete_answer(answer_id, **kwargs):
    answer = kwargs['answer']
    form = answer.form
    answer.delete()
    form.expired = form.has_expired()
    form.save()
    author = form.author
    if author.set_disk_usage_alert():
        author.save()
    form.add_log(_("Deleted an answer"))
    if flask_login.current_user.id == author.id and \
       author.total_uploads_usage() < author.uploads_limit:
        remove_alert = {
            "disk_alert": "disk-usage-alert-item",
            "alerts": "user-alerts"
        }
    else:
        remove_alert = False
    return jsonify(deleted=True, remove_alert=remove_alert), 200
