"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from liberaforms.views.errors import errors_bp
from liberaforms.views.main import main_bp
from liberaforms.views.user import user_bp
from liberaforms.views.media import media_bp
from liberaforms.views.form import form_bp
from liberaforms.views.form_style import form_style_bp
from liberaforms.views.public_form import public_form_bp
from liberaforms.views.invite import invite_bp
from liberaforms.views.site import site_bp
from liberaforms.views.admin import admin_bp
from liberaforms.views.answers import answers_bp
from liberaforms.views.consent import consent_bp
from liberaforms.views.data_display import data_display_bp
from liberaforms.api.api import api_bp
