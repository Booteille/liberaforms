/*
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
*/

var required_file_fields = []

function loadFormWithAnswers() {
  const previous_answers = {{ previous_answer|tojson }}

    for (const [field_id, field_value] of Object.entries(previous_answers)) {
      // console.log(field_id, field_value)
      if (field_id.startsWith("checkbox-group-") || field_id.startsWith("radio-group-")) {
        field_value.split(", ").forEach(option => {
          $(".field-"+field_id).find("input[value='"+option+"']").prop("checked", true)
        })
        continue
      }
      if (field_id.startsWith("file-")) {
        if ($("#"+field_id).prop("required")) {
          $("#"+field_id).removeAttr('required')
          $("#"+field_id).parsley('removeConstraint', 'required')

          required_file_fields.push(field_id)
        }
        $("#"+field_id).hide()
        $(".field-"+field_id).find(".form-text").hide()
        let file_name = $(field_value).text();
        let dummy = dummy_file_template.replace(/%file_field_id%/g, field_id)
                                       .replace(/%file_name%/g, file_name)
        $(".field-"+field_id).find("label").after(dummy)
        continue
      }
      if (field_id.startsWith("consent-") && field_value == true) {
        $("#"+field_id).prop("checked", true)
        continue
      }
      $("#"+field_id).val(field_value)
    }
    {% if form.might_send_confirmation_email() %}
    if ($("form").find("input[name='{{form.confirmation_field_name}}']").length) {
      let email_field = $("form").find("input[type=email]:first")
      if (email_field.length) {
        updateConfirmationMessage(email_field.val())
      }
    }
    {% endif %}
}
function replaceDummyWithFileField(button) {
  let file_field_id = $(button).attr("file-field-id")
  $(button).closest(".dummy_file").remove()
  if (required_file_fields.includes(file_field_id)) {
    $("#"+file_field_id).prop("required", true)
    $("#"+file_field_id).parsley('addConstraint', 'required')
  }
  $("#"+file_field_id).show()
  $(".field-"+file_field_id).find(".form-text").show()

}
const dummy_file_template = `
<div class="dummy_file">
  <p class="mb-1">%file_name%</p>
  <input type="hidden" name="dummy-%file_field_id%" value="%file_field_id%" />
  <button type="button" class="btn btn-sm btn-outline-primary mt-2" file-field-id="%file_field_id%" onclick="javascript:replaceDummyWithFileField(this)">{{ _("Edit file") }}</button>
</div>
`
