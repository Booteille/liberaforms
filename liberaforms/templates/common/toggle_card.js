
function _toggleCard(card) {
  let make_visible = !card.find('.toggled-content').is(':visible')
  if (make_visible == true) {
    card.find('.toggle-card').html(`<i data-feather="chevron-up" width="24" height="24" stroke-width="2" aria-hidden="true"></i>`)
    card.find('.toggled-content').slideDown(150)
  } else {
    card.find('.toggle-card').html(`<i data-feather="chevron-down" width="24" height="24" stroke-width="2" aria-hidden="true"><i>`)
    card.find('.toggled-content').slideUp(150)
  }
  feather.replace();
  let card_name = card.prop('id')
  $.ajax({
    url : "{{ url_for('form_bp.toggle_card_visibility', form_id=form.id) }}",
    type: "POST",
    dataType: "json",
    data: { preference_key: card_name, is_visible: make_visible },
    beforeSend: function(xhr, settings) {
      if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type)) {
        xhr.setRequestHeader("X-CSRFToken", csrftoken)
      }
    },
    success: function(data, textStatus, jqXHR)
    {

    }
  });
}
$(".toggle-card").click(function() {
  let card = $(this).closest('.card')
  _toggleCard(card)
  return false;
})
