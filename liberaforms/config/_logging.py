"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later

https://medium.com/tenable-techblog/the-boring-stuff-flask-logging-21c3a5dd0392
https://github.com/tenable/flask-logging-demo/tree/master/app_factory_pattern
"""

#
# !! This code works but does not work when run with multiple gunicorn workers !!
#

import os
from logging.config import dictConfig

# CRITICAL:50 <- ERROR:40 <- WARNING:30 <- INFO:20 <- DEBUG:10

DEFAULT_LOG_FORMAT = {
    "format": "[%(asctime)s.%(msecs)03d] %(levelname)s %(name)s:%(funcName)s: %(message)s",
    "datefmt": "%d/%b/%Y:%H:%M:%S",
}


LOG_MAX_BYTES = 100_000_000
LOG_COPIES = 5

class LogSetup(object):
    def __init__(self, app=None, **kwargs):
        if app is not None:
            self.init_app(app, **kwargs)

    def init_app(self, app):
        if app.config["FLASK_CONFIG"] == "development" or app.config["FLASK_CONFIG"] == "testing":
            app.config["LOG_TYPE"] = "stream"
            app.config["LOG_LEVEL"] = "DEBUG"
        log_type = app.config["LOG_TYPE"]
        logging_level = app.config["LOG_LEVEL"] or "INFO"
        log_directory = app.config["LOG_DIR"]

        os.makedirs(log_directory, exist_ok=True)
        app_log = "/".join([log_directory, "app.log"])
        www_log = "/".join([log_directory, "www.log"])

        if log_type == "stream":
            logging_policy = "logging.StreamHandler"
        elif log_type == "watched":
            logging_policy = "logging.handlers.WatchedFileHandler"
        else:
            log_max_bytes = LOG_MAX_BYTES
            log_copies = LOG_COPIES
            logging_policy = "logging.handlers.RotatingFileHandler"

        # print("log_type", log_type)
        # print("logging_level", logging_level)
        # print("logging_policy", logging_policy)

        std_format = {
            "formatters": {
                "default": DEFAULT_LOG_FORMAT,
                "access": {"format": "%(message)s"},
            }
        }

        std_logger = {
            "loggers": {
                "": {"level": logging_level, "handlers": ["default"], "propagate": True},
                "app.access": {
                    "level": logging_level,
                    "handlers": ["access_logs"],
                    "propagate": False,
                },
                "liberaforms": {
                    "level": logging_level,
                    "handlers": ["default"],
                    "propagate": False,
                },
                "gunicorn": {"level": app.config["LOG_LEVEL"], "handlers": ["default"]},
                "root": {"level": logging_level, "handlers": ["default"]},
            }
        }

        if log_type == "stream":
            logging_handler = {
                "handlers": {
                    "default": {
                        "level": logging_level,
                        "formatter": "default",
                        "class": logging_policy,
                    },
                    "access_logs": {
                        "level": logging_level,
                        "class": logging_policy,
                        "formatter": "access",
                    },
                }
            }
        elif log_type == "watched":
            logging_handler = {
                "handlers": {
                    "default": {
                        "level": logging_level,
                        "class": logging_policy,
                        "filename": app_log,
                        "formatter": "default",
                        "delay": True,
                    },
                    "access_logs": {
                        "level": logging_level,
                        "class": logging_policy,
                        "filename": www_log,
                        "formatter": "access",
                        "delay": True,
                    },
                }
            }
        else:
            logging_handler = {
                "handlers": {
                    "default": {
                        "level": logging_level,
                        "class": logging_policy,
                        "filename": app_log,
                        "backupCount": log_copies,
                        "maxBytes": log_max_bytes,
                        "formatter": "default",
                        "delay": True,
                    },
                    "access_logs": {
                        "level": logging_level,
                        "class": logging_policy,
                        "filename": www_log,
                        "backupCount": log_copies,
                        "maxBytes": log_max_bytes,
                        "formatter": "access",
                        "delay": True,
                    },
                }
            }

        log_config = {
            "version": 1,
            "formatters": std_format["formatters"],
            "loggers": std_logger["loggers"],
            "handlers": logging_handler["handlers"],
        }
        dictConfig(log_config)
