import os
from dotenv import load_dotenv

directory = "/app"
load_dotenv(dotenv_path=".env")

command = 'gunicorn'
bind = '0.0.0.0:5000'
workers = os.environ["GUNICORN_WORKERS"]
