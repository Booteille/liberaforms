"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2022 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
from datetime import datetime, timezone
import markdown
from sqlalchemy.dialects.postgresql import JSONB, TIMESTAMP, ENUM
from sqlalchemy.ext.mutable import MutableDict
from sqlalchemy.orm.attributes import flag_modified
from jinja2 import Template
from flask import current_app, g
from flask_babel import gettext as _
from flask_babel import force_locale
from liberaforms import db
from liberaforms.utils.database import CRUD
from liberaforms.utils import utils
from liberaforms.utils import sanitizers
from liberaforms.utils import i18n

#from pprint import pprint

class Consent(db.Model, CRUD):

    __tablename__ = "consents"
    id = db.Column(db.Integer, primary_key=True, index=True)
    created = db.Column(TIMESTAMP, nullable=False)
    name = db.Column(db.String, default="", nullable=False)
    label = db.Column(MutableDict.as_mutable(JSONB), nullable=False)
    text = db.Column(MutableDict.as_mutable(JSONB), nullable=False)
    checkbox_label = db.Column(MutableDict.as_mutable(JSONB), nullable=False)
    required = db.Column(db.Boolean, default=False, nullable=False)
    shared = db.Column(db.Boolean, default=False, nullable=False)
    enforced = db.Column(db.Boolean, default=False, nullable=False)
    site_id = db.Column(db.Integer, db.ForeignKey('site.id'), nullable=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=True)
    form_id = db.Column(db.Integer, db.ForeignKey('forms.id', ondelete="CASCADE"), nullable=True)
    template_id = db.Column(db.Integer, nullable=True)
    wizard = db.Column(MutableDict.as_mutable(JSONB), nullable=False)
    lang_code = None  # used by schemas.consent to dump fields with the same lang_code

    def __init__(self, **kwargs):
        self.created = datetime.now(timezone.utc)
        self.name = kwargs["name"] if "name" in kwargs else ""
        self.label = kwargs["label"] if "label" in kwargs else {}
        self.text = kwargs["text"] if "text" in kwargs else {}
        self.required = kwargs["required"] if "required" in kwargs else True
        self.checkbox_label = kwargs["checkbox_label"] if "checkbox_label" in kwargs else {}
        self.wizard = kwargs["wizard"] if "wizard" in kwargs else {}
        self.site_id = kwargs["site_id"] if "site_id" in kwargs else None
        self.user_id = kwargs["user_id"] if "user_id" in kwargs else None
        self.form_id = kwargs["form_id"] if "form_id" in kwargs else None
        self.template_id = kwargs["template_id"] if "template_id" in kwargs else None

    #def save(self):
    #    super().save()
    #    print(utils.print_obj_values(self))

    def __str__(self):
        """Use for debugging."""
        return utils.print_obj_values(self)

    def save_texts(self, lang_code, **kwargs) -> None:
        self.label[lang_code] = kwargs["label"]
        kwargs.pop('label')
        if self.label[lang_code] == "":
            self.label.pop(lang_code, None)
        flag_modified(self, "label")
        self.checkbox_label[lang_code] = kwargs["checkbox_label"]
        kwargs.pop('checkbox_label')
        flag_modified(self, "checkbox_label")
        self.text[lang_code] = {}
        self.text[lang_code]["markdown"] = kwargs["md_text"]
        self.text[lang_code]["html"] = sanitizers.markdown_to_html(kwargs["md_text"])
        flag_modified(self, "text")
        kwargs.pop('md_text')
        for key, value in kwargs.items():
            setattr(self, key, value)
        self.save()

    def __str__(self) -> str:
        return utils.print_obj_values(self)

    @classmethod
    def find(cls, **kwargs):
        return cls.find_all(**kwargs).first()

    @classmethod
    def find_all(cls, **kwargs):
        return cls.query.filter_by(**kwargs).order_by(cls.id.desc())

    @staticmethod
    def validate(form: dict, consents: list) -> bool:
        """Check all consents are present (checked) in request.form."""
        for consent in consents:
            if consent.required and consent.field_name not in form.keys():
                return False
        return True

    @classmethod
    def copy(cls, consent, mark_as_copy=False):
        def _copy(original: dict) -> dict:
            new_value = original.copy()
            if g.language not in new_value:
                text = i18n.get_best_language_value(new_value, g.language)
                new_value[g.language] = text
            return new_value
        # i18n: refers to a copy of a privacy statement
        name = _("Copy of '%(name)s'", name=consent.name) if mark_as_copy else consent.name
        return Consent(
                name=name,
                label=_copy(consent.label),
                text=_copy(consent.text),
                checkbox_label=_copy(consent.checkbox_label),
                required=consent.required,
                wizard=consent.wizard.copy(),
                template_id=consent.id)

    def get_best_lang_code(self) -> str:
        """Render this consent with this lang_code."""

        def translation_exists(lang_code):
            # we only test for self.text because it a required wtform field
            return lang_code in self.text and \
                   "markdown" in self.text[lang_code] and \
                   self.text[lang_code]['markdown']

        lang_code = g.language
        if lang_code in self.text and translation_exists(lang_code):
            return lang_code
        agent_languages = i18n.parse_request_accept_language()
        for lang_code in agent_languages:
            if lang_code in self.text and translation_exists(lang_code):
                return lang_code
        lang_code = g.site.language
        if lang_code in self.text and translation_exists(lang_code):
            return lang_code
        available_languages = i18n.all_enabled_site_languages()
        for lang_code in available_languages:
            if lang_code in self.text and translation_exists(lang_code):
                return lang_code
        return None

    @property
    def field_name(self) -> str:
        """String used in HTML forms and form.field_index"""
        return f"consent-{self.id}-check"

    @staticmethod
    def label_examples() -> dict:
        """Return all consent label placeholders used in edit consent forms."""
        placeholders = {}
        for lang in current_app.config["LANGUAGES"].keys():
            with force_locale(lang):
                placeholders[lang] = _('eg. Privacy statement')
        return placeholders

    @staticmethod
    def placeholders() -> dict:
        """Return all placeholders used in edit consent forms."""
        placeholders = {}
        for lang in current_app.config["LANGUAGES"].keys():
            with force_locale(lang):
                placeholders[lang] = _('I agree')
        return placeholders

    @staticmethod
    def wizard_disclaimer(organization_link: str) -> str:
        file_path = os.path.join(current_app.config['ASSETS_DIR'],
                                 f"gdpr-wizard/disclaimer.{g.language}.md")
        if not os.path.isfile(file_path):
            file_path = os.path.join(current_app.config['ASSETS_DIR'],
                                     "gdpr-wizard/disclaimer.en.md")
        with open(file_path, 'r', encoding="utf-8") as disclaimer_doc:
            disclaimer_md = disclaimer_doc.read()
            template = Template(disclaimer_md)
            disclaimer_md = template.render(organization_name=organization_link)
            disclamer_html = markdown.markdown(disclaimer_md)
        return disclamer_html

    def get_copy_count(self) -> int:
        return Consent.find_all(template_id=self.id).count()

    def delete(self):
        if self.site_id:
            from liberaforms.models.site import Site
            site = Site.find()
            if self.id in site.registration_consent:
                site.registration_consent.remove(self.id)
                site.save()
        super().delete()
