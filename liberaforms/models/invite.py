"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from datetime import datetime, timezone
from sqlalchemy.dialects.postgresql import JSONB, TIMESTAMP, UUID
from sqlalchemy.ext.mutable import MutableDict
from flask import current_app, g, url_for
from flask_babel import gettext as _
from flask_babel import force_locale
from liberaforms.utils.database import CRUD
from liberaforms import db
from liberaforms.models.user import User
from liberaforms.utils import utils
from liberaforms.utils import sanitizers
from liberaforms.utils.tokens import str_time_format

#from pprint import pprint

class Invite(db.Model, CRUD):
    """Invite model definition."""

    __tablename__ = "invites"
    id = db.Column(db.Integer, primary_key=True, index=True)
    created = db.Column(TIMESTAMP, nullable=False)
    last_sent = db.Column(TIMESTAMP, nullable=True)
    email = db.Column(db.String, nullable=False)
    message = db.Column(db.String, nullable=True)
    token = db.Column(MutableDict.as_mutable(JSONB), nullable=False)
    role = db.Column(db.String, default="editor", nullable=False)
    invited_by_id = db.Column(db.Integer, nullable=True)
    granted_form = db.Column(JSONB, nullable=True)  # Invitation grants access to this form
    ldap_uuid = db.Column(UUID, nullable=True)  # The invited user can be found on the LDAP DIT

    def __init__(self, **kwargs):
        """Create a new Invite object."""
        self.created = datetime.now(timezone.utc)
        self.email = kwargs["email"]
        self.message = kwargs["message"]
        self.token = kwargs["token"]
        self.role = kwargs["role"]
        self.invited_by_id = kwargs["invited_by_id"]
        self.granted_form = kwargs["granted_form"] if 'granted_form' in kwargs else None
        self.ldap_uuid = kwargs["ldap_uuid"] if 'ldap_uuid' in kwargs else None

    def __str__(self):
        """Use for debugging."""
        return utils.print_obj_values(self)

    @classmethod
    def find(cls, **kwargs):
        """Return first Invite filtered by kwargs."""
        return cls.find_all(**kwargs).first()

    @classmethod
    def find_all(cls, **kwargs):
        """Return all Invites filtered by kwargs."""
        filters = []
        if 'token' in kwargs:
            filters.append(cls.token.contains({'token': kwargs['token']}))
            kwargs.pop('token')
        if 'granted_form' in kwargs:
            filters.append(cls.granted_form.contains({'id': kwargs['granted_form']}))
            kwargs.pop('granted_form')
        for key, value in kwargs.items():
            filters.append(getattr(cls, key) == value)
        return cls.query.filter(*filters).order_by(Invite.created.desc())

    def get_message(self) -> str:
        """Return Invite message."""
        return self.message

    def get_inviter(self):
        """Return User object."""
        if not self.invited_by_id:
            return None
        user = User.find(id=self.invited_by_id)
        return user if user else None

    def has_ldap_entry(self) -> bool:
        """When the invited email address exists on the LDAP DIT."""
        return bool(self.ldap_uuid)

    def renew_token_life(self):
        """Set the token.created time to now. Make the token last more time."""
        if "created" in self.token:
            created = datetime.now(timezone.utc).strftime(str_time_format)
            self.token["created"] = created
            self.save()

    @staticmethod
    def default_message(lang: str) -> str:
        """Return default Invite message."""
        with force_locale(lang):
            # i18n: Default message for email invitation.
            return _("Hello,\n\nYou have been invited to %(site_name)s.\n\nUse this link to activate your account:\n[LINK]\n\nRegards.\n%(site_name)s",
                     site_name=g.site.name)

    @staticmethod
    def get_invitation_url(token: str) -> str:
        """Return URL to Invite."""
        token = token if token else "60d8618d6aa95b47e3b9ad1cdbeab"
        return url_for("user_bp.create_new_user", invite=token, _external=True)

    @staticmethod
    def prepare_message(text: str, invite_url=None, replace_vars=True) -> str:
        if replace_vars:
            text = text.replace("[SITE_NAME]", g.site.name)
            text = text.replace("[LINK]", invite_url)
        return sanitizers.remove_html_tags(text)

    @staticmethod
    def get_preview(text: str) -> str:
        url = Invite.get_invitation_url(token="")
        text = Invite.prepare_message(text, url)
        text = utils.nl2br(text)
        return text

    @staticmethod
    def placeholders() -> dict:
        """Return translated placeholders used in invitation forms."""
        placeholders = {}
        for lang in g.site.custom_languages:
            placeholders[lang] = Invite.default_message(lang)
        return placeholders

    @staticmethod
    def previews() -> dict:
        previews = {}
        for lang in g.site.custom_languages:
            if lang in g.site.invitation_text.keys() \
               and g.site.invitation_text[lang]:
                text = g.site.invitation_text[lang]
            else:
                text = Invite.default_message(lang)
            previews[lang] = Invite.get_preview(text)
        return previews
