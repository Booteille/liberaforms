"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2022 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from flask_babel import gettext as _
from flask import url_for
from liberaforms import ma
from liberaforms.models.invite import Invite
from liberaforms.utils import utils


class InviteSchema(ma.SQLAlchemySchema):
    class Meta:
        model = Invite

    id = ma.Integer()
    created = ma.Method('get_created')
    last_sent = ma.Method('get_last_sent')
    email = ma.auto_field()
    message = ma.Method('get_message')
    role = ma.auto_field()
    invited_by = ma.Method('get_invited_by')
    has_granted_form = ma.Method('get_has_granted_form')

    def get_created(self, obj) -> str:
        return utils.utc_to_g_timezone(obj.created).strftime("%Y-%m-%d %H:%M:%S")

    def get_last_sent(self, obj) -> str:
        if obj.last_sent:
            return utils.utc_to_g_timezone(obj.last_sent).strftime("%Y-%m-%d %H:%M:%S")
        return "Not sent"

    def get_message(self, obj) -> str:
        return obj.get_message().replace("\n", "<br/>")

    def get_invited_by(self, obj):
        if obj.invited_by_id:
            user=obj.get_inviter()
            if user:
                url=url_for('admin_bp.inspect_user', user_id=obj.invited_by_id)
                return f"<a href='{url}'>{user.username}</a>"
        return ""

    def get_has_granted_form(self, obj) -> bool:
        return bool(obj.granted_form)
