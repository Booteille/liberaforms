"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2022 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from marshmallow import pre_dump
from flask_babel import gettext as _
from flask_babel import force_locale
from liberaforms import ma
from liberaforms.models.consent import Consent
from liberaforms.utils import utils


class ConsentSchema(ma.SQLAlchemySchema):
    class Meta:
        model = Consent
        ordered = True

    id = ma.Integer()
    created = ma.Method('get_created')
    name = ma.auto_field()
    label = ma.Method('get_label')
    text = ma.Method('get_text')
    checkbox_label = ma.Method('get_checkbox_label')
    field_name = ma.Method('get_field_name')
    required = ma.auto_field()
    shared = ma.auto_field()
    enforced = ma.auto_field()
    site_id = ma.auto_field()
    user_id = ma.auto_field()
    form_id = ma.auto_field()
    is_copy = ma.Method('get_is_copy')
    copy_count = ma.Method('get_total_copies')
    forced_lang = None

    def __init__(self, **kwargs):
        if "lang_code" in kwargs:
            self.forced_lang = kwargs['lang_code']
            del kwargs['lang_code']
        super().__init__(**kwargs)

    @pre_dump
    def set_lang_code_for_this_consent(self, obj, **kwargs):
        obj.lang_code = self.forced_lang if self.forced_lang else None
        if not obj.lang_code:
            obj.lang_code = obj.get_best_lang_code()
        return obj

    def get_created(self, obj) -> str:
        return utils.utc_to_g_timezone(obj.created).strftime("%Y-%m-%d")

    def get_label(self, obj) -> str:
        return obj.label[obj.lang_code] if obj.lang_code in obj.label else ""

    def get_text(self, obj) -> str:
        if obj.lang_code in obj.text and \
           "html" in obj.text[obj.lang_code] and \
           obj.text[obj.lang_code]['html']:
            return obj.text[obj.lang_code]["html"]
        return ""

    def get_checkbox_label(self, obj) -> str:
        label = obj.checkbox_label[obj.lang_code] if obj.lang_code in obj.checkbox_label else ""
        if not label:
            with force_locale(obj.lang_code):
                return _("I agree")
        return label

    def get_field_name(self, obj) -> str:
        return obj.field_name

    def get_is_copy(self, obj) -> bool:
        return bool(obj.template_id)

    def get_total_copies(self, obj) -> int:
        return obj.get_copy_count()
