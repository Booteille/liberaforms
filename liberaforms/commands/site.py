"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from flask import current_app
from flask.cli import with_appcontext
from flask.cli import AppGroup
import click
from liberaforms.models.site import Site
from liberaforms.domain import form as form_domain
from liberaforms.utils import validators

site_cli = AppGroup('site')


@site_cli.command()
@with_appcontext
def expire_forms():
    """Expire forms with an old expiry date condition and send email."""
    expired_forms = form_domain.expire_all_by_date_condition()
    if current_app.config["FLASK_CONFIG"] == "production":
        print(f"Expired {expired_forms} forms")
    if expired_forms:
        current_app.logger.info(f"FORM - Server cronjob expired {expired_forms} forms")


@site_cli.command()
@with_appcontext
def get_versions():
    """Print the LiberaForms and alembic versions to the terminal."""
    site = Site.find()
    print({
        "Liberaforms": site.app_version(),
        "Database schema": site.alembic_version()
    })


@site_cli.command()
@click.option('-language', multiple=True, help="Language code like 'en'")
@with_appcontext
def set(language=None):
    """Set site's custom languages."""
    site = Site.find()
    if language:
        new_languages = []
        for lang_code in language:
            if validators.is_valid_lang_code(lang_code):
                new_languages.append(lang_code)
            else:
                print(f"'{lang_code}' is not a valid lang code. Skipping.")
        site.custom_languages = new_languages if new_languages else ["en"]
        site.language = site.custom_languages[0]
        site.save()
        print(f"Set custom languages: {', '.join(site.custom_languages)}")
