"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import click
from flask import current_app
from flask.cli import AppGroup
from flask.cli import with_appcontext
from liberaforms.models.site import Site
from liberaforms.models.user import User
from liberaforms.utils import password as password_utils
from liberaforms.utils import validators

user_cli = AppGroup('user')


@user_cli.command()
@click.option('-role', 'role', help="User's role [guest|editor|admin]")
@click.argument("username")
@click.argument("email")
@click.argument("password")
@with_appcontext
def create(username, email, password, role=None):
    if not validators.is_valid_email(email):
        click.echo("Not a valid email")
        return False
    if User.find(username=username):
        click.echo("User already exists")
        return False
    if User.find(email=email):
        click.echo("User with email already exists")
        return False
    admin_settings = User.default_admin_settings()
    role = role if role else 'editor'
    user = User(username=username,
                email=email,
                password=password,
                preferences=User.default_user_preferences(),
                admin=admin_settings,
                role=role,
                validated_email=True,
                uploads_enabled=Site.find().newuser_enableuploads,
                uploads_limit=current_app.config['DEFAULT_USER_UPLOADS_LIMIT']
                )
    user.save()
    print(f'{role} created OK. id: {user.id}')
    return True

@user_cli.command()
@click.argument("username")
@with_appcontext
def disable(username):
    user = User.find(username=username)
    if not user:
        click.echo("User not found")
    user.blocked = True
    user.save()
    click.echo(f"{username} disabled")


@user_cli.command()
@click.argument("username")
@with_appcontext
def enable(username):
    user = User.find(username=username)
    if not user:
        click.echo("User not found")
    user.blocked = False
    user.save()
    click.echo(f"{username} enabled")


@user_cli.command()
@click.argument("username")
@click.option('-password', 'password')
@click.option('-email', 'email')
@with_appcontext
def set(username, password=None, email=None):
    """Set user properties."""
    user = User.find(username=username)
    save = False
    if not user:
        click.echo("User not found")
        return
    if password:
        user.password_hash = password_utils.hash_password(password)
        click.echo("Set password")
        save = True
    if email:
        if validators.is_valid_email(email):
            user.email = email
            click.echo("Set email")
            save = True
        else:
            click.echo("Not a valid email")
    if save:
        user.save()


@user_cli.command()
@click.option('-language', 'language')
@with_appcontext
def set_default(language=None):
    """Set new user defaults."""
    site = Site.find()
    if language:
        if language in current_app.config["LANGUAGES"]:
            site.newuser_language = language
            site.save()
            print(f"Set new user language to: {language}")
        else:
            print(f"'{language}' is not a valid lang code")
