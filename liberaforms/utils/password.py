"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import math
from flask import current_app
import password_strength
from passlib.hash import pbkdf2_sha256
from passlib import utils as passlib_utils
from password_strength import PasswordStats as pwdstats


password_policy = password_strength.PasswordPolicy.from_names(
    entropybits=30,
    length=8,
    uppercase=0,
    numbers=0,
    special=0,
    nonletters=0
)


def password_policy_failed(password: str) -> bool:
    if not password:
        return True
    try:
        password = passlib_utils.saslprep(password)
    except Exception as error:
        current_app.logger.debug(f"APP - {error}")
        return True
    return bool(password_policy.test(password))


def get_strength(passwd: str) -> int:
    """Get strength 1 to 4. return >= 3 is strong enough."""
    if password_policy_failed(passwd):
        return 1
    tested_password = password_policy.password(passwd)
    strength = tested_password.strength()
    if strength:
        quality = [math.ceil(step/strength) for step in [0.25, 0.54, 0.76]]
        return quality.count(1) + 1
    return 1


def hash_password(password: str) -> str:
    """Encrypt a password."""
    settings = {
        'rounds': 200000,
        'salt_size': 16,
    }
    return pbkdf2_sha256.using(**settings).hash(password)


def verify_password(password, hash: str):
    """Compare password with hash."""
    return pbkdf2_sha256.verify(password, hash)
