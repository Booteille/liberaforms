"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2022 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import unicodecsv as csv
import typing
from flask import current_app, g
from flask_babel import gettext as _
from liberaforms.models.answer import Answer


def write_users_csv(users) -> str:
    """Generate a CSV containing users data.

    Return temporary os path to the CSV.
    """
    fieldnames=["username", "created", "enabled", "email", "language", "forms", "admin"]
    fieldheaders={  "username": _("Username"),
                    "created": _("Created"),
                    # i18n: Used as column title
                    "enabled": _("Enabled"),
                    # i18n: Email, used as column title
                    "email": _("Email"),
                    # i18n: Used as column title
                    "language": _("Language"),
                    # i18n: Used as column title
                    "forms": _("Forms"),
                    # i18n: Whether user is admin
                    "admin": _("Admin")
                    }
    csv_name = os.path.join(os.environ['TMP_DIR'], f"{g.site.hostname}.users.csv")
    with open(csv_name, mode='wb') as csv_file:
        writer = csv.DictWriter(csv_file,
                                fieldnames=fieldnames,
                                extrasaction='ignore')
        writer.writerow(fieldheaders)
        for user in users:
            # i18n: Boolean option: True or False
            is_enabled = _("True") if user.enabled else _("False")
            is_admin = _("True") if user.is_admin() else _("False")
            row = { "username": user.username,
                    "created": user.created.strftime("%Y-%m-%d"),
                    "enabled": is_enabled,
                    "language": current_app.config['LANGUAGES'][user.get_language()][0],
                    "email": user.email,
                    "forms": user.get_forms().count(),
                    "admin": is_admin
                    }
            writer.writerow(row)
    return csv_name


def write_answers_csv(form, with_deleted_columns=False) -> str:
    """Generate a CSV containing the Form's answers.

    Return temporary os path to the CSV.
    """
    label_cache:typing.Dict = {}
    def get_label(field_name, answer_value) -> str:
        if answer_value in label_cache:
            return label_cache[answer_value]
        answer_label = form.get_answer_label(field_name, answer_value)
        label_cache[answer_value] = answer_label
        return label_cache[answer_value]
    fieldnames=[]
    fieldheaders={}
    for field in form.get_field_index_for_display(with_deleted_columns):
        fieldnames.append(field['name'])
        fieldheaders[field['name']]=field['label']
    csv_name = os.path.join(os.environ['TMP_DIR'], f"{form.slug}.csv")
    with open(csv_name, mode='wb') as csv_file:
        writer = csv.DictWriter(csv_file,
                                fieldnames=fieldnames,
                                extrasaction='ignore')
        writer.writerow(fieldheaders)
        answers = form.get_answers_for_display(oldest_first=True)
        for answer in answers:
            for field_name in answer.keys():
                if field_name.startswith('file-'):
                    url = Answer.get_file_field_url(answer[field_name])
                    if url:
                        answer[field_name] = url
                elif field_name.startswith("checkbox-group") or \
                     field_name.startswith("radio-group") or \
                     field_name.startswith("select"):
                    answer[field_name] = get_label(field_name, answer[field_name])
            writer.writerow(answer)
    return csv_name
