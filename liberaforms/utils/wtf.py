"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2020 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import mimetypes
import json
import pytz
import bleach
from flask_wtf import FlaskForm
from wtforms import (StringField, TextAreaField, IntegerField, SelectField,
                     PasswordField, BooleanField, RadioField, FileField,
                     HiddenField, URLField, EmailField)
from wtforms.validators import (ValidationError, DataRequired,
                                Email, EqualTo, Length)
from flask import current_app, g
from flask_babel import gettext
from flask_babel import lazy_gettext as _
import flask_login

from liberaforms.models.user import User
from liberaforms.models.form import Form
from liberaforms.utils import sanitizers
from liberaforms.utils import utils
from liberaforms.utils import validators
from liberaforms.utils import password as password_utils


def raise_password_strength_error(passwd):
    """Password validation not handled by WTForms."""
    if password_utils.get_strength(passwd) < 3:
        raise ValidationError(_("The password is not strong enough"))


class NewUser(FlaskForm):
    username = StringField(_("Username"), validators=[DataRequired(),
                                                      Length(min=4, max=16)])
    email = EmailField(_("Email"), validators=[DataRequired(), Email()])
    password = PasswordField(_("Password"), validators=[DataRequired()])
    password2 = PasswordField(_("Confirm"), validators=[EqualTo('password'),
                                                        DataRequired()])

    def validate_username(self, username):
        if username.data != sanitizers.sanitize_username(username.data):
            raise ValidationError(_("Username is not valid"))
        if username.data in current_app.config['RESERVED_USERNAMES']:
            raise ValidationError(_("Please use a different username"))
        if User.find(username=username.data):
            raise ValidationError(_("Please use a different username"))

    def validate_email(self, email):
        if User.find(email=email.data):
            raise ValidationError(_("Please use a different email address"))

    def validate_password(self, password):
        if not password.errors:
            if len(password.data) > 128:
                raise ValidationError(_("Password is too long. Max 128"))
            raise_password_strength_error(password.data)


class Login(FlaskForm):
    username = StringField(_("Username"), validators=[DataRequired()])
    password = PasswordField(_("Password"), validators=[DataRequired()])

    def validate_username(self, username):
        if username.data != sanitizers.sanitize_username(username.data):
            return False


class DeleteAccount(FlaskForm):
    delete_username = StringField(_("Enter your username"), validators=[DataRequired()])
    delete_password = PasswordField(_("Enter your password"), validators=[DataRequired()])

    def validate_delete_username(self, delete_username):
        if delete_username.data != flask_login.current_user.username:
            raise ValidationError(_("That is not your username"))

    def validate_delete_password(self, delete_password):
        if not password_utils.verify_password(delete_password.data,
                                          flask_login.current_user.password_hash):
            raise ValidationError(_("That is not your password"))


class GetEmail(FlaskForm):
    email = EmailField(_("Email"), validators=[DataRequired(), Email()])


class NewEmailAddress(FlaskForm):
    email = EmailField(_("Email"), validators=[DataRequired(), Email()])

    def validate_email(self, email):
        if flask_login.current_user.is_root_user():
            # i18n: '.env' is the name of a specific file
            raise ValidationError(_("You are the root user. See the .env file"))
        if User.query.filter(User.email == email.data,
                             User.id != flask_login.current_user.id).first():
            raise ValidationError(_("Please use a different email address"))


class ResetPassword(FlaskForm):
    password = PasswordField(_("New password"), validators=[DataRequired()])
    password2 = PasswordField(_("Password again"),
                              validators=[DataRequired(), EqualTo('password')])

    def validate_password(self, password):
        if not password.errors:
            if len(password.data) > 128:
                raise ValidationError(_("Password is too long. Max 128"))
            raise_password_strength_error(password.data)


class smtpConfig(FlaskForm):
    host = StringField(_("Email server"), validators=[DataRequired()])
    port = IntegerField(_("Port"), validators=[DataRequired()])
    encryption = SelectField(_("Encryption"),
                             choices=[('None', 'None'),
                                      ('SSL', 'SSL'),
                                      ('STARTTLS', 'STARTTLS')],
                             validators=[DataRequired()])
    user = StringField(_("User"))
    password = StringField(_("Password"))
    noreplyAddress = StringField(_("Sender address"),
                                 validators=[DataRequired(), Email()])


class NewInvite(FlaskForm):
    email = EmailField(_("New user's email"), validators=[DataRequired(), Email()])
    language = SelectField(_("Language"))
    message = TextAreaField(_("Invitation message"))
    granted_form_id = HiddenField()
    role = SelectField(_("New user role"),
                       choices=[('guest', _("Guest")),
                                ('editor', _("Editor")),
                                ('admin', _("Admin"))])

    def validate_message(self, message):
        message.data = sanitizers.remove_html_tags(message.data)
        if "[LINK]" not in message.data:
            raise ValidationError(_("The variable [LINK] is required"))

    def validate_language(self, language):
        if language.data not in current_app.config['LANGUAGES'].keys():
            raise ValidationError(_("Not a valid language"))

    def validate_email(self, email):
        if User.find(email=email.data) or email.data == current_app.config['ROOT_USER']:
            raise ValidationError(_("Please use a different email address"))

    def validate_role(self, role):
        if role.data not in ('guest', 'editor', 'admin'):
            raise ValidationError("Not a valid role")

    def validate_granted_form_id(self, granted_form_id):
        if granted_form_id.data:
            try:
                granted_form_id.data = int(granted_form_id.data)
                if not Form.find(id=granted_form_id.data):
                    raise ValidationError("Not a valid form id")
            except Exception as exc:
                raise ValidationError("Not a valid form id") from exc


class InvitationText(FlaskForm):
    text = TextAreaField(_("Invitation message"))
    language = SelectField(_("Language"))

    def validate_text(self, field):
        if field.data:
            field.data = sanitizers.remove_html_tags(field.data)
            if "[LINK]" not in field.data:
                raise ValidationError(_("The variable [LINK] is required"))

    def validate_language(self, field):
        if field.data not in g.site.custom_languages:
            raise ValidationError(_("Not a valid language"))


class ConsentText(FlaskForm):
    name = StringField(_("A name (for your convenience)"), validators=[DataRequired()])
    label = StringField(_("Title"))
    md_text = TextAreaField(_("Statement text"), validators=[DataRequired()])
    checkbox_label = StringField(_("Agreement text"))
    required = HiddenField()
    wizard = HiddenField()
    language_selector = SelectField(_("Language"))  # only site consents have language_selector
    language = HiddenField()  # the language_selector field populates this field

    def validate_name(self, field):
        field.data = sanitizers.remove_html_tags(field.data)
        if not field.data:
            raise ValidationError(_("Not a valid name"))

    def validate_label(self, field):
        if field.data:
            field.data = sanitizers.remove_html_tags(field.data)
            if not field.data:
                raise ValidationError(_("Not a valid label"))

    def validate_md_text(self, field):
        field.data = sanitizers.remove_html_tags(field.data)
        if not field.data.strip():
            raise ValidationError(_("That text was not valid"))

    def validate_checkbox_label(self, field):
        if field.data:
            field.data = sanitizers.remove_html_tags(field.data)

    def validate_language(self, field):
        languages = list(current_app.config['LANGUAGES'].keys()) + g.site.custom_languages
        if field.data not in languages:
            raise ValidationError(_("Not a valid language"))


class ChangeTheme(FlaskForm):
    font_color = HiddenField(validators=[DataRequired()])
    primary_color = StringField(_("HTML color code"), validators=[DataRequired()])

    def validate_font_color(self, font_color):
        if font_color.data not in ["#ddd", "#333"]:
            font_color.data = "#ddd"

    def validate_primary_color(self, primary_color):
        if not validators.is_hex_color(primary_color.data):
            raise ValidationError(_("Not a valid HTML color code"))


class ChangeTimeZone(FlaskForm):
    timezone = StringField(_("Time zone"), validators=[DataRequired()])

    def validate_timezone(self, timezone):
        if timezone.data not in pytz.common_timezones:
            raise ValidationError(_("Not a valid time zone"))


class FileExtensions(FlaskForm):
    # i18n: Refers to file extensions, such as .odt or .pdf
    extensions = TextAreaField(_("A list of file extensions. One per line."), validators=[DataRequired()])

    def validate_extensions(self, extensions):
        mimetype_risks_file = os.path.join(current_app.config['ASSETS_DIR'], 'mimetype_risks.json')
        # https://plugins.trac.wordpress.org/browser/pro-mime-types/trunk/promimetypes.php
        with open(mimetype_risks_file, 'r', encoding="utf-8") as all_risks:
            risks = json.load(all_risks)
        dangerous_extensions = list(risks["dangerous"].keys())
        mimetypes.init()
        for ext in extensions.data.splitlines():
            if not ext:
                continue
            mimetype, encoding = mimetypes.guess_type(f"file_name.{ext}")
            if ext in dangerous_extensions:
                raise ValidationError(_("Will not enable %(extension)s", extension=ext))
            if not mimetype:
                raise ValidationError(_("Unknown file extension %(extension)s", extension=ext))


class UploadMedia(FlaskForm):
    """Posted via JSON."""
    alt_text = StringField(_("Descriptive text"),
                           validators=[DataRequired(), Length(min=12, max=100)])
    media_file = FileField(_("Select a file"))  # not required at common/_insert-image-modal

    def validate_media_file(self, field):
        if not field.data:
            raise ValidationError(gettext("File required"))
        if "image/" not in field.data.content_type:
            raise ValidationError(gettext("Not a valid image file"))
        field.data.seek(0, os.SEEK_END)
        file_size = field.data.tell()
        field.data.seek(0, 0)
        max_size = current_app.config['MAX_MEDIA_SIZE']
        if file_size > max_size:
            max_size = utils.human_readable_bytes(max_size)
            # i18n: %(number)s is a digital size. Example: 50 MB
            err_msg = _("File too big. Maximum size is %(number)s", number=max_size)
            raise ValidationError(err_msg)


class UserUploadLimit(FlaskForm):
    size = StringField(validators=[DataRequired()])
    unit = StringField(validators=[DataRequired()])

    def validate_size(self, field):
        try:
            size = float(field.data)
        except:
            raise ValidationError(_("Must be a number"))
        if size <= 0:
            raise ValidationError(_("Must be greater the zero"))


class FediverseAuth(FlaskForm):
    title = StringField(_("Connection name (for your convenience)"),
                        validators=[DataRequired()])
    node_url = URLField(_("Fediverse node"),
                        default="",
                        validators=[DataRequired()])
    access_token = HiddenField(_("Access token"),
                               default="",
                               validators=[DataRequired()])

    def validate_title(self, field):
        field.data = sanitizers.remove_html_tags(field.data).strip()
        if not field.data:
            raise ValidationError(_("That text was not valid"))

    def validate_node_url(self, field):
        if field.data != field.data.strip():
            raise ValidationError(_("That was not a valid URL"))
        if field.data and not validators.is_valid_url(field.data):
            raise ValidationError(_("That was not a valid URL"))


class NewForm(FlaskForm):
    name = StringField(_("The name of your form"),
                       validators=[DataRequired(), Length(min=5, max=50)])
    slug = StringField(_("Form address"),
                       validators=[DataRequired(), Length(min=5, max=50)])

    def validate_name(self, field):
        field.data = sanitizers.remove_html_tags(field.data).strip()
        if not field.data:
            raise ValidationError(_("Not a valid name"))

    def validate_slug(self, field):
        field.data = sanitizers.sanitize_slug(field.data).strip()
        if not field.data or Form.find(slug=field.data) or \
           field.data in current_app.config['RESERVED_SLUGS']:
            raise ValidationError(_("Please use another"))


class FormName(FlaskForm):
    name = StringField(_("The new form name"),
                       validators=[DataRequired(), Length(min=5, max=42)])

    def validate_name(self, field):
        field.data = sanitizers.remove_html_tags(field.data).strip()
        if not field.data:
            raise ValidationError(_("Not a valid name"))

class FormPublish(FlaskForm):
    image_source = StringField()
    text = TextAreaField(validators=[DataRequired()])


class FormShortDescription(FlaskForm):
    short_desc = TextAreaField(_("Short description"), validators=[DataRequired()])


class ContactInformation(FlaskForm):
    contact_info = TextAreaField(_("Other information"))
    language = SelectField(_("Language"))

    def validate_contact_info(self, field):
        field.data = sanitizers.bleach_text(field.data).strip()
        field.data = bleach.linkify(field.data, skip_tags=None, parse_email=False)

    def validate_language(self, field):
        if field.data not in g.site.custom_languages:
            raise ValidationError(_("Not a valid language"))


class SiteBlurb(FlaskForm):
    blurb = TextAreaField(_("Text"))
    language = SelectField(_("Language"))

    def validate_blurb(self, field):
        field.data = sanitizers.remove_html_tags(field.data).strip()

    def validate_language(self, field):
        if field.data not in g.site.custom_languages:
            raise ValidationError(_("Not a valid language"))


class ResourcesMenu(FlaskForm):
    menu = HiddenField(validators=[DataRequired()])  # empty = []
    language = SelectField(_("Language"))

    def validate_menu(self, field):
        try:
            menu = json.loads(field.data)
            for resource in menu:
                if len(resource) != 2:
                    raise ValidationError(_("That was not a valid menu"))
                resource[0] = sanitizers.remove_html_tags(resource[0]).strip()
                if not resource[0]:
                    raise ValidationError(_("That was not a valid menu"))
                if not (resource[1].startswith("http://") or resource[1].startswith("https://")):
                    raise ValidationError(_("That was not a valid menu"))
            field.data = json.dumps(menu)
        except Exception as exc:
            raise ValidationError(_('That was not a valid menu')) from exc

    def validate_language(self, field):
        if field.data not in g.site.custom_languages:
            raise ValidationError(_("Not a valid language"))


class NewFormMessage(FlaskForm):
    msg = StringField(_("One line of text"))
    language = SelectField(_("Language"))

    def validate_msg(self, field):
        field.data = sanitizers.bleach_text(field.data).strip()   # TODO: could remove all tags execpt <a>
        if not field.data:
            raise ValidationError(_("That text was not valid"))

    def validate_language(self, field):
        if field.data not in g.site.custom_languages:
            raise ValidationError(_("Not a valid language"))


class OrganizationProfile(FlaskForm):
    name = StringField(_("The name of your organization"), validators=[DataRequired()])
    url = URLField(_("Privacy policy web page"))
    tos_url = URLField(_("Terms of Service web page"))
    email = EmailField(_("Contact email"))

    def validate_name(self, field):
        field.data = sanitizers.remove_html_tags(field.data).strip()
        if not field.data:
            raise ValidationError(_("That text was not valid"))

    def validate_url(self, field):
        if field.data != field.data.strip():
            raise ValidationError(_("That was not a valid URL"))
        if field.data and not validators.is_valid_url(field.data):
            raise ValidationError(_("That was not a valid URL"))

    def validate_tos_url(self, field):
        if field.data != field.data.strip():
            raise ValidationError(_("That was not a valid URL"))
        if field.data and not validators.is_valid_url(field.data):
            raise ValidationError(_("That was not a valid URL"))

    def validate_email(self, field):
        if field.data != field.data.strip():
            raise ValidationError(_("That was not a valid email"))
        if field.data and not validators.is_valid_email(field.data):
            raise ValidationError(_("That was not a valid email"))



class DataProtectionLaw(FlaskForm):
    law = StringField(_("The name of the law used by the wizard."),
                      validators=[DataRequired()])

    def validate_law(self, field):
        field.data = sanitizers.remove_html_tags(field.data).strip()
        if not field.data:
            raise ValidationError(_("That text was not valid"))
