"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2020 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import re
import html
import markdown
from unidecode import unidecode
from flask import url_for, current_app, flash, render_template, redirect, request
from flask_babel import gettext as _
import flask_login
import bleach


def sanitize_string(string):
    string = unidecode(string)
    string = string.replace(" ", "")
    return re.sub('[^A-Za-z0-9\-\.]', '', string)  # DeprecationWarning: invalid escape sequence \-


def sanitize_slug(slug) -> str:
    """Return a valid URL path string."""
    slug = slug.lower().strip().strip(".")
    slug = re.sub(' +', ' ', slug)
    slug = slug.strip()
    while "  " in slug:
        slug = slug.replace("  ", " ")
    slug = slug.replace(" ", "-")
    slug = slug.strip("-")
    max_slug_length = 50
    return sanitize_string(slug)[:max_slug_length]


def sanitize_token(token) -> bool:
    return sanitize_string(token)


def sanitize_username(username):
    return sanitize_string(username)


def remove_html_tags(text: str) -> str:
    """Remove tags"""
    text = html.unescape(text)
    TAG_RE = re.compile(r'<[^>]+>')
    return TAG_RE.sub('', text).strip()


def markdown_to_html(markdown_text: str) -> str:
    markdown_text = remove_html_tags(markdown_text.strip())
    _html = markdown.markdown(markdown_text, extensions=['markdown.extensions.nl2br'])
    return bleach.linkify(_html, skip_tags=None, parse_email=False)


def sanitize_label(text: str) -> str:
    text = text.replace("_", "-")  # _ is a special char, see form_helper._get_unique_value
    text = remove_html_tags(text)
    text = remove_newlines(text)
    return text.strip()


def bleach_text(text: str) -> str:
    text = bleach.clean(text,
                        tags=['a', 'p', 'br'],  # allowed
                        attributes={'a': ['href', 'rel', 'target'], 'p': [], 'br': []},
                        strip=True,
                        strip_comments=True)
    return text if remove_html_tags(text).strip() else ""


def remove_newlines(string: str) -> str:
    string = string.replace("\n", "")
    return string.replace("\r", "")


def remove_first_and_last_newlines(string: str) -> str:
    RE = "^[\r\n]+|[\r\n]+$"
    return re.sub(RE, '', string)


def truncate_text(text: str, truncate_at: int = 155) -> str:
    """Return first n chars of text.

    155 is the recommened opengraph length
    """
    text = text.strip('\n').replace('  ', ' ').strip(' ')
    if len(text) > truncate_at:
        text = f"{text[0:truncate_at-3]}..."
    return text
