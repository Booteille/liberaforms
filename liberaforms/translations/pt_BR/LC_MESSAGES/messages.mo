��    e      D              l  #   m     �     �     �     �     �     �     �     �  	   �     �     �               &     6     G     X     _     n     z     �  U   �            
        !     A  	   F     P     W     _     e     r  
   z     �     �  	   �     �  
   �     �     �     �     �  '   �     �     	     	     %	     5	     A	  
   S	     ^	     d	     m	     y	     	     �	     �	     �	     �	     �	     �	     �	     �	  	   �	     �	     �	      
     
     1
     ?
  
   F
     Q
     c
     h
     m
     ~
  	   �
     �
     �
  
   �
     �
  k   �
     R     X     f     w     �     �     �     �     �     �  $   �     �     �  2     0   7  x   h  �  �  #   �     �  	   �     	               3  	   <     F  
   R     ]     k     {     �     �     �     �     �     �     �  $         %  U   E     �     �     �  %   �     �     �     �                  
   @     K     T     ]  	   c     m     y     �     �  	   �  	   �  2   �  
   �               )     =     F     `     n     w     ~     �  
   �     �     �     �     �     �     �     �     �               -     ?  $   Y     ~     �     �     �     �     �     �     �     �     �  #        :      G  k   h     �     �     �        
        !     3     E     X  	   i  ,   s     �     �  6   �  /   �  x   ,   %(total_uploads)s of %(disk_limit)s Admin Answers Author Average Blocked by admin Cancel Change Change author Configure Confirmation Create an account Created Current author's username Data protection Default language Default timezone Delete Delete answers Delete user Delete user and authored forms Delete user and forms Deleted '%(form_name)s' and one answer Deleted '%(form_name)s' and %(number)s answers Details Disabled Disk usage Dont show me this message again Edit Edit mode Editor Editors Email Email server Enabled Expiration Expired False Fediverse Form Form title Forms Front page text Graphs Guest Helpful text only visible to your users I understand Installation Invitation message Invitation only Invitations Invite a new user Invited by Label Language Last answer Login Look and feel Marked Maximum Media Minimum Multiple choice My forms Name New author's username New forms New user Numeric fields One byte %(number)s bytes Please try again soon. Primary color Public Public URL Restricted access Role Save Set upload limit Site configuration Site name Site settings Sorry, this form has expired. Statistics Super Admin options This form has been posted on the Fediverse once This form has been posted on the Fediverse %(number)s times Total Total answers Total disk usage Total forms True Uploads enabled Uploads limit Use default value Username Users Users will see this friendly message Validated email Version We're sorry, this form is temporarily unavailable. You are going to change the author of this form. Your form %(form_name)s is shared with one person. Your form %(form_name)s is shared with %(people_count)s other people. Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: info@liberaforms.org
POT-Creation-Date: 2023-10-03 17:06+0200
PO-Revision-Date: 2022-04-08 07:14+0000
Last-Translator: J. Lavoie <j.lavoie@net-c.ca>
Language: pt_BR
Language-Team: Portuguese (Brazil) <https://hosted.weblate.org/projects/liberaforms/server-liberaforms/pt_BR/>
Plural-Forms: nplurals=2; plural=n > 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.10.3
 %(total_uploads)s do %(disk_limit)s Administrador Respostas Autor Média Bloqueado pelo Administrador Cancelar Modificar Mudar autor Configurar Confirmação Criar uma conta Desenvolvido Nome do autor atual Proteção de Dados Idioma Padrão Fuso horário padrão Deletar Deletar respostas Deletar usuário Deletar usuário e seus formulários Deletar usuário e formulários Deleted '%(form_name)s' and one answer Deleted '%(form_name)s' and %(number)s answers Detalhes Desabilitado Uso do disco Não mostrar está mensagem novamente Editar Modo de Edição Editor Editores Correio Eletrônico Servidor de correio eletrônico Habilitado Validade Expirado Falso Fediverse Formulário Título do formulário Formulários Texto da página inicial Gráficos Convidado Texto de ajuda visível apenas para seus usuários Eu entendo Instalação Mensagem do convite Somente por convite Convites Convidar um novo usuário Convidado por Etiqueta Idioma Última resposta Entrar Aparência Marcado Máximo Meios de comunicação Mínimo Múltipla escolha Meus formulários Nome Novo nome do autor Novos Formulários Novo Usuário Campos numéricos One byte %(number)s bytes Por favor, tente novamente em brave. Cor primária Público URL público Acesso restrito Função Salvar Definir limite de envios Configuração do site Nome do Site Configuração do site Desculpe, esse formulário expirou. Estatística Opições de Super Administrador This form has been posted on the Fediverse once This form has been posted on the Fediverse %(number)s times Total Total de respostas Uso total do disco Total de Formulários Verdadeiro Envios Habilitado Limites de Envios Usar valor padrão Nome do Usuário Usuários Usuários receberão esta mensagem amigável Correio eletrônico Validado Versão Lamentamos, formulário temporariamente indisponível. Você irá modificar o autor desse formulário. Your form %(form_name)s is shared with one person. Your form %(form_name)s is shared with %(people_count)s other people. 