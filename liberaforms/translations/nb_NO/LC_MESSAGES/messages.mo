��    �      �              	  *   	     8	     E	     K	     \	     v	     ~	     �	     �	     �	     �	     �	  2   �	     
     
     $
     9
     I
  	   [
     e
     r
     �
     �
  #   �
     �
     �
          
     #     =     M     ^     o     v     �     �     �     �     �     �     �     �                    $  
   )     4     G     M     [  
   o     z     �  
   �     �     �  	   �  "   �     �               "     3     @     L     \     e     m     }     �     �     �     �     �     �     �     �     �  
                        *     =     M     _     h     v  	   {     �  	   �     �     �     �     �     �     �       
                  0     7     @     Y     h     {     �     �     �     �     �     �  
   �     �       	          	   2     <     J  
   Z     e     l  k   �     �  	   �               $     0     5     >     V     l     r     z  T   �  t   �  3   M  x   �  �  �  .   �          (     6     G     Z     _     f     �     �     �     �  A   �          !     .     B     P     a     j     v  
   �     �  )   �     �     �  	         
     '     F     V     f     x     ~  
   �     �     �     �     �     �     �     �                     )     1     ?     T     [     i  
   y     �     �     �     �     �  
   �     �     �               "     0     A     Q     ^     f     r     �     �     �     �     �     �     �     �       
             !  
   &     1     J     Y     b     �     �     �     �  	   �     �     �     �     �            
   &  	   1     ;     Q     ]     q     w     �     �     �     �     �     �     �                    2     >     N     _     g     s     |     �  
   �     �     �  k   �     *     .     7     J     Z     g  
   n     y     �     �     �     �  T   �  t   !  +   �  x   �   A photo of a horse galloping on the beach. Access token Admin An error occured An image file is required Answers Author Bad credentials Blocked by admin Cancel Cannot delete root user Cannot delete yourself Cannot delete. You are the only Admin on this site Change Change author Change email address Change password Changed author OK Configure Confirmation Confirmation message Congratulations! Connected to the Fediverse Could not encrypt your access token Couldn't find that token Create an account Created Current author incorrect Current author's username Data protection Default language Default timezone Delete Delete account Delete answers Delete form Delete my account Delete user Delete user and forms Deleted all answers Deleted an answer Descriptive text Details Disable Disabled Edit Edit again Edited expiry text Email Email address Email notifications Encryption Enter your password Enter your username Expiration Expiration conditions Expired Fediverse Fediverse configuration deleted OK Fediverse node Forgot your password? Form Form attachments Form created Form edited Front page text Generate Go back HTML color code I agree Installation Introduction text Invitation message Invitation only Invitations Invite a new user Label Language Last answer Learn more Log Login Missing date or time Modified an answer Multiple choice My admin settings My forms My statistics Name New forms New user New users Not a valid HTML color code Not a valid time zone Please use a different username Port Preview Primary color Public Public URL Recover Recover password Remove Required Required file is missing Reset password SMTP config works! Save Save changes Saved form OK Search for country or city name Select a file Send Send a new user invitation Send email Sender address Short description Show more Site configuration Site name Site settings Site statistics Statistics Submit That is not your password This form has been posted on the Fediverse once This form has been posted on the Fediverse %(number)s times Time Time zone Time zone changed OK Upload a file Use default User Username Username does not match Username is not valid Users Version View log You are going to delete one answer You are going to delete %(total_answers)s answers You are going to delete this form and its unique answer You are going to delete this form and its %(number)s answers You will delete your account and the following data Your form %(form_name)s is shared with one person. Your form %(form_name)s is shared with %(people_count)s other people. Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: info@liberaforms.org
POT-Creation-Date: 2023-10-03 17:06+0200
PO-Revision-Date: 2021-11-10 17:04+0000
Last-Translator: LiberaForms <info@liberaforms.org>
Language: nb_NO
Language-Team: Norwegian Bokmål <https://hosted.weblate.org/projects/liberaforms/server-liberaforms/nb_NO/>
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.10.3
 Et bilde av en hest galopperende på en strand Tilgangssymbol Administrator En feil inntraff En bildefil kreves Svar Skaper Feilaktige identitetsdetaljer Blokkert av administrator Avbryt Kan ikke slette rot-bruker Kan ikke slette deg selv Kan ikke slette. Du er den eneste administratoren på denne siden Endre Endre skaper Endre e-postadresse Endre passord Skaper endret OK Sett opp Bekreftelse Bekreftelsesmelding Gratulerer Tilkoblet til Fediverset Kunne ikke kryptere tilgangssymbolet ditt Fant ikke det symbolet Opprett en konto Opprettet Feilaktig nåværende skaper Nåværende skapers brukernavn Databeskyttelse Forvalgt språk Forvalgt tidssone Slett Slett konto Slett svar Slett skjema Slett kontoen min Slett bruker Slett bruker og skjemaer Slettet alle svar Slettet ett svar Beskrivende tekst Detaljer Skru av Avskrudd Rediger Rediger igjen Rediger utløpstekst E-post E-postadresse E-postmerknader Kryptering Skriv inn passordet ditt Skriv inn brukernavnet Utløp Utløpsvilkår Utløpt Fediverset Fediversoppsett slettet OK Fediversnode Glemt passordet ditt? Skjema Skjemavedlegg Skjema opprettet Skjema redigert Forsidetekst Generer Gå tilbake HTML-fargemodus Jeg samtykker Installasjonstekst Introduksjonstekst Invitasjonsmelding Kun ved invitasjon Invitasjoner Inviter en ny bruker Etikett Språk Siste svar Lær mer Logg Innlogging Manglende dato eller tid Endret et svar Flervalg Mine administratorinnstillinger Mine skjema Min statistikk Navn Nye skjemaer Ny bruker Nye brukere Ikke en gyldig HTML-fargekode Ikke en gyldig tidssone Bruk et annet brukernavn Port Forhåndsvis Hovedfarge Offentlig Offentlig nettadresse Gjenopprett Gjenopprett passord Fjern Påkrevd Påkrevd fil mangler Tilbakestill passord SMTP-oppsett virker. Lagre Lagre endringer Lagret skjema OK Søk etter lands- eller bynavn Velg en fil Send Send en ny brukerinvitasjon Send e-post Avsenderadresse Kort beskrivelse Vis mer Sideoppsett Sidenavn Sideinnstillinger Sidestatistikk Statistikk Send inn Feil passord This form has been posted on the Fediverse once This form has been posted on the Fediverse %(number)s times Tid Tidssone Tidssone endret OK Last opp en fil Bruk forvalg Bruker Brukernavn Brukernavn stemmer ikke overens Ugyldig brukernavn Brukere Versjon Vis loggføring You are going to delete one answer You are going to delete %(total_answers)s answers You are going to delete this form and its unique answer You are going to delete this form and its %(number)s answers Du vil slette kontoen din og følgende data Your form %(form_name)s is shared with one person. Your form %(form_name)s is shared with %(people_count)s other people. 