��                        �     �  1        5     <     C     J     R  U   Y  -   �     �     �     �     �     �                      5   0     f     n  
   s  k   ~     �     �     �  T     t   V  x   �  2  D  
   w  N   �  
   �     �     �     �     	  �     H   �     �     	  
   	  
   	     (	     ;	     D	     M	  *   T	  T   	     �	     �	      
  �   
     �
     �
     �
  �     �   �  �   <   Admin Attached to one form Attached to %(number)s forms Author Cancel Change Created Delete Deleted '%(form_name)s' and one answer Deleted '%(form_name)s' and %(number)s answers Deleted one answer Deleted %(number)s answers Edit Enabled Form Forms Installation Language Login Name One byte %(number)s bytes One pending invitation %(number)s pending invitations Preview Save Statistics This form has been posted on the Fediverse once This form has been posted on the Fediverse %(number)s times Username Users Version You are going to delete one answer You are going to delete %(total_answers)s answers You are going to delete this form and its unique answer You are going to delete this form and its %(number)s answers Your form %(form_name)s is shared with one person. Your form %(form_name)s is shared with %(people_count)s other people. Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: info@liberaforms.org
POT-Creation-Date: 2023-10-03 17:06+0200
PO-Revision-Date: 2021-09-08 20:50+0000
Last-Translator: Artem <Localizer_in_Russian@protonmail.com>
Language: ru
Language-Team: Russian <https://hosted.weblate.org/projects/liberaforms/server-liberaforms/ru/>
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.10.3
 Админ Attached to one form Attached to %(number)s forms Attached to %(number)s forms Автор Отмена Изменить Создано Удалить Deleted '%(form_name)s' and one answer Deleted '%(form_name)s' and %(number)s answers Deleted '%(form_name)s' and %(number)s answers Deleted one answer Deleted %(number)s answers Deleted %(number)s answers Редактировать Включено Форма Формы Установка Язык Вход Имя One byte %(number)s bytes %(number)s bytes One pending invitation %(number)s pending invitations %(number)s pending invitations Предпросмотр Сохранить Статистика This form has been posted on the Fediverse once This form has been posted on the Fediverse %(number)s times This form has been posted on the Fediverse %(number)s times Имя пользователя Пользователи Версия You are going to delete one answer You are going to delete %(total_answers)s answers You are going to delete %(total_answers)s answers You are going to delete this form and its unique answer You are going to delete this form and its %(number)s answers You are going to delete this form and its %(number)s answers Your form %(form_name)s is shared with one person. Your form %(form_name)s is shared with %(people_count)s other people. Your form %(form_name)s is shared with %(people_count)s other people. 