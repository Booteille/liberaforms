# LiberaForms2 upgrade

> Use this documentation to upgrade from LiberaForms1 to LiberaForms2

## Migrate the database

First, follow and complete the instructions at https://gitlab.com/liberaforms/mongo2postgres

## Clone LiberaForms

You can install LiberaForms in the directory of your choice.
Please install version2 in a new, empty directory.

```
apt-get install git python3-venv
git clone https://gitlab.com/liberaforms/liberaforms.git
cd liberaforms
python3 -m venv ./venv
```

## Configuration

The configuration has changed. The file is now called `.env`

```
cp dotenv.example .env
```

### Edit `.env` and adjust

* The `DB_USER` must be the same as the database username your used during the mongo - postgres migration

There are new variables to be set in the `.env` file. Please read through them.

Note the `ROOT_USERS=[]` has changed to `ROOT_USER=""` (only one ROOT_USER)

## Create the database

Create a user and database with the `.env` values

```
flask database create
```

Another way to do the same is this
```
sudo su
su postgres -c "liberaforms/commands/postgres.sh create-db"
exit
```

## Populate the database

Load your migrated postgresql dump file into the newly created database
```
sudo su
su postgres
cat <sql_dump_file> | psql -U postgres <db_name>
```

And then upgrade the database to the latest version
```
flask db upgrade
```

## Gunicorn and Supervisor

You may need to make changes. See `INSTALL.md`

## Nginx

Nginx is much faster serving static files than Flask.
We have added new locations to take load off the LiberaForms app.

See `docs/nginx.example` and adjust your nginx config.


## Caveats

The favicon file type has been changed to `ico`. You need to upload your site icon (`png` or `jpg`) again. It will be converted to `ico` for you.
