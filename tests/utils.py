"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import uuid
import json
from io import BytesIO
from bs4 import BeautifulSoup
import mimetypes
import werkzeug
#import flask_login
from flask import url_for
from faker import Faker


def login(client, credentials):
    response = client.post('/user/login', data=dict(
        username=credentials['username'],
        password=credentials['password']
    ), follow_redirects=True)
    #html = response.data.decode()
    #print(html)
    assert 'href="/user/logout"' in response.data.decode()
    #assert flask_login.current_user.username == credentials['username']
    return response


def logout(client):
    response = client.get('/user/logout', follow_redirects=True)
    assert '<!-- site_index_page -->' in response.data.decode()
    return response


def random_slug() -> str:
    return str(uuid.uuid4())


def username_with_len(faked_username) -> str:
    # POST "user_bp.create_new_user" requires a username between 4 and 16 chars long
    # as defined in wtf.py with the "Length(min=4, max=16)]" validator
    # But I cant see how to guarantee this with faker.
    # Workaround..
    length = len(faked_username)
    if length < 4 or length > 16:
        return f"0123456789123456{faked_username}"[-16:]
    return faked_username


def get_unique_username() -> str:
    # Tests sometimes fail because Faker generates exiting usernames
    from liberaforms.models.user import User
    fake = Faker()
    username = username_with_len(fake.user_name())
    while User.find(username=username):
        username = username_with_len(fake.user_name())
    return username


def get_unique_email() -> str:
    from liberaforms.models.user import User
    fake = Faker()
    email = fake.email()
    while User.find(email=email):
        email = fake.email()
    return email


def get_form_structure(with_email=False, with_attachment=False):
    structure = [
        {
            "type": "text",
            "required": False,
            "label": "Name",
            "className": "form-control",
            "name": "text-1620232883208",
            "subtype": "text"
        },
        {
            "type": "date",
            "required": False,
            "label": "Date",
            "className": "form-control",
            "name": "date-1620224710459"
        },
        {
            "type": "number",
            "required": False,
            "label": "Number",
            "className": "form-control",
            "name": "number-1620224716308"
        }
    ]
    if with_attachment:
        structure.append({
              "className": "form-control",
              "label": "Upload a file",
              "multiple": False,
              "name": "file-1622045746136",
              "required": False,
              "subtype": "file",
              "type": "file"
        })
    if with_email:
        structure.append({
              "type": "text",
              "subtype": "email",
              "required": False,
              "label": "Email",
              "className": "form-control",
              "name": "text-1620232903350"
        })
    return json.dumps(structure)


def create_file_obj(file_name=None):
    if file_name:
        file_path = f"./assets/{file_name}"
        mimetype = mimetypes.guess_type(file_path)[0]
        with open(file_path, 'rb') as f:
            stream = BytesIO(f.read())
        file_obj = werkzeug.datastructures.FileStorage(
            stream=stream,
            filename=file_name,
            content_type=mimetype,
        )
    else:
        file_obj = werkzeug.datastructures.FileStorage(
            stream=None,
            filename="",
            content_type=None,
        )
    return file_obj


def get_ldap_users() -> dict:
    users = {}
    entries_file = "./assets/ldap/entries.json"
    with open(entries_file, "r", encoding="utf-8") as read_file:
        entries = json.load(read_file)
    entry_cnt = 0
    for entry in entries['entries']:
        attributes = entry["attributes"]
        if "inetOrgPerson" in attributes["structuralObjectClass"] \
           and not attributes["entryDN"] == os.environ['LDAP_BIND_ACCOUNT']:
            data = {
                "username": attributes["uid"][0],
                "email": attributes["mail"][0],
                "password": attributes["userPassword"][0]
            }
            users[entry_cnt] = data
            entry_cnt += 1
    return users


def count_errors(html):
    count = 0
    soup = BeautifulSoup(html, features="lxml")
    error_containers = soup.find_all("div", class_="error-messages")
    for container in error_containers:
        count += len(container.find_all("span", class_="wtf-error"))
    return count


#def populate_ldap_mock_server() -> None:
#    """Connect to a real LDAP server and download config into ./assets/ldap
#       This function is not used. It's just here if we need to repopulate the assets
#    """
#    import ldap3
#    REAL_SERVER="ldap://127.0.0.1:1666"
#    REAL_USER="cn=nobody,dc=example,dc=com"
#    REAL_PASS="hello"
#    server = ldap3.Server(REAL_SERVER, get_info=ldap3.ALL)
#    connection = ldap3.Connection(server, REAL_USER, REAL_PASS, auto_bind=True)
#    server.info.to_file('./assets/ldap/server_info.json')
#    server.schema.to_file('./assets/ldap/server_schema.json')
#    if connection.search('dc=example,dc=com', '(objectclass=*)',
#                          attributes=ldap3.ALL_ATTRIBUTES,
#                          get_operational_attributes=True):
#        connection.response_to_file('./assets/ldap/entries.json', raw=True)
#    connection.unbind()
