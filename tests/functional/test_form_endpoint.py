"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from flask import url_for
from liberaforms.models.formuser import FormUser
from liberaforms.models.formauth import FormAuth
from liberaforms.utils import validators
from tests.factories import FormFactory
from tests import user_creds
from tests.utils import login, logout, random_slug

class TestFormApiAuth():

    @classmethod
    def setup_class(cls):
        cls.properties = {}

    def test_requirements(self, editor):
        form = FormFactory(author=editor, slug=random_slug())
        form.save()
        FormUser(form=form, user=editor, is_editor=True).save()
        self.properties['form'] = form

    def test_auth(self, client):
        "Test form_bp.toggle_endpoint_auth."
        logout(client)
        form = self.properties['form']
        response = client.post(
                        url_for('form_bp.toggle_endpoint_auth', form_id=form.id),
                    )
        assert response.status_code == 401
        logout(client)
        login(client, user_creds['admin'])
        response = client.post(
                        url_for('form_bp.toggle_endpoint_auth', form_id=form.id),
                    )
        assert response.status_code == 401
        logout(client)
        login(client, user_creds['editor'])

    def test_auth_toggle(self, client):
        form = self.properties['form']
        assert not form.endpoint_auth
        response = client.post(
                        url_for('form_bp.toggle_endpoint_auth', form_id=form.id),
                    )
        assert response.status_code == 200
        assert form.endpoint_auth.enabled
        assert validators.is_valid_UUID(form.endpoint_auth.token)
        response = client.post(
                        url_for('form_bp.toggle_endpoint_auth', form_id=form.id),
                    )
        assert response.status_code == 200
        assert not form.endpoint_auth.enabled

    def test_delete_form(self):
        form = self.properties['form']
        assert FormAuth.find(form_id=form.id)
        form.delete()
        assert not FormAuth.find(form_id=form.id)
