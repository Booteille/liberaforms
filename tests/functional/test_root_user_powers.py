"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import uuid
import pytest
from flask import url_for
from liberaforms.models.formuser import FormUser
from tests.factories import FormFactory
from tests.factories import UserFactory
from tests import user_creds
from tests.utils import login, logout, random_slug

class TestRootUserPowers():

    @classmethod
    def setup_class(cls):
        cls.properties = {}
        cls.new_author = UserFactory(validated_email=True)
        cls.new_author.save()

    def test_requirements(self, editor, root_user):
        form = FormFactory(author=editor, slug=random_slug())
        form.save()
        FormUser(user=editor, form=form, is_editor=True).save()
        self.properties['form'] = form

    def test_auth(self, client):
        """Test admin_bp.change_author."""
        logout(client)
        form = self.properties['form']
        response = client.get(
                            url_for('admin_bp.change_author', form_id=form.id),
                            follow_redirects=True
                        )
        assert response.status_code == 200
        assert '<!-- login_to_continue -->' in response.data.decode()

        logout(client)
        login(client, user_creds['editor'])
        response = client.get(
                            url_for('admin_bp.change_author', form_id=form.id),
                            follow_redirects=True
                        )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()

        logout(client)
        login(client, user_creds['root_user'])

    def test_change_author(self, client):
        """ Tests nonexistent username and valid username
            Tests permission
        """
        form = self.properties['form']
        response = client.get(
                            url_for('admin_bp.change_author', form_id=form.id),
                            follow_redirects=False
                        )
        assert response.status_code == 200
        assert '<!-- change_author_page -->' in response.data.decode()
        initial_author = form.author
        assert FormUser.find(form_id=form.id, user_id=form.author_id) != None
        nonexistent_username = "nonexistent"
        response = client.post(
                            url_for('admin_bp.change_author', form_id=form.id),
                            data={
                                "old_author_username": initial_author.username,
                                "new_author_username": nonexistent_username,
                            },
                            follow_redirects=False
                        )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- change_author_page -->' in html
        assert "alert alert-warning" in html

        # TODO. Test change author to the already existing author
        #assert g.current.username != initial_author.username

        response = client.post(
                            url_for('admin_bp.change_author', form_id=form.id),
                            data={
                                "old_author_username": initial_author.username,
                                "new_author_username": self.new_author.username,
                            },
                            follow_redirects=True
                        )
        assert response.status_code == 200
        html = response.data.decode()
        assert 'alert alert-success' in html
        assert '<!-- admin_form_details_page -->' in html
        assert self.properties['form'].author.id == self.new_author.id
