"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pytest
from flask import url_for
from liberaforms.models.user import User
from liberaforms.models.formuser import FormUser
from tests.factories import FormFactory
from tests import user_creds
from tests.utils import login, logout, random_slug

class TestFormAdminSettings():
    @classmethod
    def setup_class(cls):
        cls.properties = {}

    def test_requirements(self, editor, client):
        form = FormFactory(author=editor, slug=random_slug())
        form.enabled = True
        form.save()
        FormUser(form=form, user=editor, is_editor=True).save()
        self.properties['editor'] = editor
        self.properties['form'] = form

    def test_disable_form(self, client):
        """ Tests disable form
            Test permissions
        """
        logout(client)
        response = client.get(
                            f"/{self.properties['form'].slug}",
                            follow_redirects=False
                        )
        assert response.status_code == 200  # the form is public
        form_id = self.properties['form'].id
        response = client.post(
                            url_for('admin_bp.disable_form', form_id=form_id),
                            follow_redirects=True
                        )
        assert response.status_code == 200
        assert '<!-- login_to_continue -->' in response.data.decode()

        login(client, user_creds['editor'])
        response = client.post(
                            url_for('admin_bp.disable_form', form_id=form_id),
                            follow_redirects=True
                        )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()

        login(client, user_creds['admin'])
        response = client.post(
                            url_for('admin_bp.disable_form', form_id=form_id),
                            follow_redirects=True
                        )
        assert response.status_code == 200
        assert '<!-- admin_form_details_page -->' in response.data.decode()
        assert self.properties['form'].admin_preferences['public'] is False
        assert self.properties['form'].is_public() is False

        logout(client)
        response = client.get(
                            f"/{self.properties['form'].slug}",
                            follow_redirects=True
                        )
        assert response.status_code == 404
        assert '<!-- page_not_found_404 -->' in response.data.decode()
        # clean up
        self.properties['form'].admin_preferences['public'] = True
        self.properties['form'].save()
