"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pytest
import json
from flask import url_for
import flask_login
from liberaforms.models.site import Site
from liberaforms.models.consent import Consent
from liberaforms.models.formconsent import FormConsent
from liberaforms.models.formuser import FormUser
from tests.factories import ConsentFactory
from tests.factories import FormFactory
from tests import user_creds
from tests import utils
from tests.utils import login, logout


class TestSiteConsentEnforce():

    @classmethod
    def setup_class(cls):
        cls.properties = {}
        cls.site = Site.find()

    def test_requirements(self, editor):
        consent = ConsentFactory(site_id=self.site.id)
        consent.save()
        assert consent.site_id
        assert not consent.enforced
        assert Consent.find(site_id=self.site.id, id=consent.id)
        self.properties["consent"] = consent
        form = FormFactory(author=editor, slug=utils.random_slug())
        form.enabled = True
        form.requires_consent = True
        form.save()
        FormUser(user=editor, form=form, is_editor=True).save()
        assert not form.is_public()
        self.properties["form"] = form

    def test_auth(self, client):
        """Test consent_bp.toggle_enforced_consent
                consent_bp.delete_site_data_consent."""

        consent = self.properties["consent"]

        logout(client)
        response = client.post(
                        url_for('consent_bp.toggle_enforced_consent', consent_id=consent.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 401
        response = client.post(
                        url_for('consent_bp.delete_site_data_consent'),
                        follow_redirects=False,
                    )
        assert response.status_code == 401

        login(client, user_creds["editor"])
        response = client.post(
                        url_for('consent_bp.toggle_enforced_consent', consent_id=consent.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 401
        response = client.post(
                        url_for('consent_bp.delete_site_data_consent'),
                        follow_redirects=False,
                    )
        assert response.status_code == 401

        login(client, user_creds["admin"])

    def test_toggle_force_consent_1(self, editor, client):
        consent = self.properties["consent"]
        form = self.properties["form"]
        assert consent.enforced is False
        assert len(form.get_consents()) == 0
        response = client.post(
                        url_for('consent_bp.toggle_enforced_consent',
                                consent_id=consent.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert consent.enforced is True
        assert consent in form.get_consents()
        assert form.is_public()

    def test_toggle_force_consent_2(self, editor, client):
        consent = self.properties["consent"]
        form = self.properties["form"]
        assert consent.enforced is True
        assert len(form.get_consents()) == 1
        response = client.post(
                        url_for('consent_bp.toggle_enforced_consent',
                                consent_id=consent.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert consent.enforced is False
        assert len(form.get_consents()) == 0
        assert form.requires_consent is False
        assert form.is_public()

    def test_delete_enforced_consent(self, client):
        consent = self.properties["consent"]
        consent.enforced = True
        consent.save()
        response = client.post(
                        url_for('consent_bp.delete_site_data_consent'),
                        data={
                            "consent_id": consent.id,
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 406

    def test_delete_consent(self, client):
        consent = self.properties["consent"]
        consent.enforced = False
        consent.save()
        response = client.post(
                        url_for('consent_bp.delete_site_data_consent'),
                        data={
                            "consent_id": consent.id,
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert response.json['deleted'] is True
        assert not Consent.find(id=consent.id)
