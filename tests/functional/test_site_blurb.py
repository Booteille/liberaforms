"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
from flask import url_for
from liberaforms.models.site import Site
from tests.utils import login, logout
from tests import user_creds



class TestSiteBlurb():

    @classmethod
    def setup_class(cls):
        cls.site = Site.find()

    def test_auth(self, editor, client):
        "Test site_bp.edit_blurb."
        logout(client)
        response = client.get(
                        url_for("site_bp.edit_blurb"),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- login_to_continue -->' in response.data.decode()

        login(client, user_creds['editor'])
        response = client.get(
                        url_for("site_bp.edit_blurb"),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()

        logout(client)
        login(client, user_creds['admin'])
        response = client.get(
                        url_for("site_bp.edit_blurb"),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert '<!-- edit_blurb_page -->' in response.data.decode()

    def test_edit_blurb(self, client):
        """Post markdown and tests resulting HTML and short_text."""
        response = client.post(
                    url_for("site_bp.edit_blurb"),
                    data={
                        'blurb': "# Tested !!\nline1\nline2",
                        "language": self.site.language
                    },
                    follow_redirects=True,
                )
        assert response.status_code == 200
        front_page = self.site.blurb["front_page"][self.site.language]
        assert '<h1>Tested !!</h1>' in front_page['html']
        assert '# Tested !!' in front_page['markdown']
        assert 'Tested !!' in self.site.blurb['short_text']
