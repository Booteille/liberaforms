"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pytest
import json
import flask_login
from flask import url_for
from liberaforms.models.formuser import FormUser
from tests.factories import FormFactory
from tests import user_creds
from tests.utils import login, logout, random_slug, get_form_structure

class TestFormExpirationSettings():

    @classmethod
    def setup_class(cls):
        cls.properties = {}

    def test_requirements(self, editor):
        structure = get_form_structure()
        form = FormFactory(author=editor,
                           slug=random_slug(),
                           structure=json.loads(structure))
        form.save()
        FormUser(form=form, user=editor, is_editor=True).save()
        self.properties['form'] = form

    def test_auth(self, client):
        """Test form_bp.expiration,
                form_bp.toggle_form_expiration_notification
                form_bp.set_expiration_date
                form_bp.set_expiry_total_answers
                form_bp.set_expiry_field_condition."""

        form = self.properties['form']
        logout(client)
        response = client.get(
                        url_for('form_bp.expiration', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- login_to_continue -->' in response.data.decode()
        response = client.post(
                        url_for('form_bp.toggle_form_expiration_notification', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 401
        assert response.is_json is True
        response = client.post(
                        url_for('form_bp.set_expiration_date', form_id=form.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 401
        assert response.is_json is True
        response = client.post(
                        url_for('form_bp.set_expiry_total_answers', form_id=form.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 401
        assert response.is_json is True
        response = client.post(
                        url_for('form_bp.set_expiry_field_condition', form_id=form.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 401
        assert response.is_json is True

        login(client, user_creds['admin'])
        response = client.get(
                        url_for('form_bp.expiration', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()
        response = client.post(
                        url_for('form_bp.toggle_form_expiration_notification', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 401
        assert response.is_json is True
        response = client.post(
                        url_for('form_bp.set_expiration_date', form_id=form.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 401
        assert response.is_json is True
        response = client.post(
                        url_for('form_bp.set_expiry_total_answers', form_id=form.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 401
        assert response.is_json is True
        response = client.post(
                        url_for('form_bp.set_expiry_field_condition', form_id=form.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 401
        assert response.is_json is True
        logout(client)
        login(client, user_creds['editor'])
        response = client.get(
                        url_for('form_bp.expiration', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- form_expiration_page -->' in response.data.decode()


    def test_toggle_expiration_notification(self, editor, client):
        form = self.properties['form']
        form_user = FormUser.find(form_id=form.id,
                                  user_id=editor.id)
        initial_preference = form_user.notifications['expiredForm']
        response = client.post(
                        url_for('form_bp.toggle_form_expiration_notification', form_id=form.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert response.json['notification'] != initial_preference
        assert initial_preference != form_user.notifications['expiredForm']

    def test_set_expiration_date(self, client):
        """Test invalid date, past date, future date, remove date condition."""
        form = self.properties['form']
        initial_log_count = self.properties['form'].log.count()
        invalid_date = "2021-00-00"
        response = client.post(
                        url_for('form_bp.set_expiration_date', form_id=form.id),
                        data={
                            "date": invalid_date,
                            "time": "00:00"
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert response.json['expired'] is False
        assert form.has_expired() is False
        assert form.can_expire() is False
        assert form.log.count() == initial_log_count
        valid_past_date = "2021-01-01"
        response = client.post(
                        url_for('form_bp.set_expiration_date', form_id=form.id),
                        data = {
                            "date": valid_past_date,
                            "time": "00:00"
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.json['expired'] is True
        assert form.has_expired() is True
        assert form.can_expire() is True
        assert form.log.count() == initial_log_count + 1
        initial_log_count = form.log.count()
        valid_future_date = "2121-05-05"
        response = client.post(
                        url_for('form_bp.set_expiration_date', form_id=form.id),
                        data={
                            "date": valid_future_date,
                            "time": "00:00"
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.json['expired'] is False
        assert form.has_expired() is False
        assert form.can_expire() is True
        assert form.log.count() == initial_log_count + 1
        initial_log_count = form.log.count()
        response = client.post(
                        url_for('form_bp.set_expiration_date', form_id=form.id),
                        data={
                            "date": "",
                            "time": ""
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.json['expired'] is False
        assert form.has_expired() is False
        assert form.can_expire() is False
        assert form.log.count() is initial_log_count + 1

    def test_set_max_answers_expiration(self, client, expiry_conditions):
        """ Tests valid max answers exipry condition value
            Tests invalid max answers exipry condition value
        """
        form = self.properties['form']
        initial_log_count = form.log.count()
        valid_max_answers = expiry_conditions['max_answers']
        response = client.post(
                        url_for('form_bp.set_expiry_total_answers', form_id=form.id),
                        data={
                            "total_answers": valid_max_answers,
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert response.json['total_answers'] == valid_max_answers
        assert form.has_expired() is False
        assert form.can_expire() is True
        assert form.log.count() == initial_log_count + 1
        initial_log_count = form.log.count()
        invalid_max_answers = "invalid_integer"
        response = client.post(
                        url_for('form_bp.set_expiry_total_answers', form_id=form.id),
                        data={
                            "total_answers": invalid_max_answers,
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert response.json['total_answers'] == 0
        assert form.has_expired() is False
        assert form.can_expire() is False
        assert form.expiry_conditions['totalAnswers'] == 0
        assert form.log.count() == initial_log_count + 1
        initial_log_count = form.log.count()
        response = client.post(
                        url_for('form_bp.set_expiry_total_answers', form_id=form.id),
                        data={
                            "total_answers": invalid_max_answers,
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert form.has_expired() is False
        assert form.can_expire() is False
        assert form.log.count() == initial_log_count + 1

    def test_set_max_number_field_expiration(self, client, expiry_conditions):
        """ Tests for max_number_field input in html
            Tests valid max answers exipry condition value
            Tests invalid max answers exipry condition value
        """
        form = self.properties['form']
        # ./tests/assets/valid_form_structure.json contains a number field
        # with id number-1620224716308
        number_field_id = "number-1620224716308"
        response = client.get(
                        url_for('form_bp.expiration', form_id=form.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert f'<input id="{number_field_id}"' in response.data.decode()

        initial_log_count = form.log.count()
        valid_max_number = expiry_conditions['number_field_max']
        response = client.post(
                        url_for('form_bp.set_expiry_field_condition', form_id=form.id),
                        data={
                            "field_name": number_field_id,
                            "condition": valid_max_number
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert response.json['condition'] == str(valid_max_number)
        assert response.json['expired'] is False
        assert form.has_expired() is False
        assert form.can_expire() is True
        field_condition = {'type': 'number', 'condition': expiry_conditions['number_field_max']}
        assert form.expiry_conditions['fields'][number_field_id] == field_condition
        assert form.log.count() == initial_log_count + 1
        initial_log_count = form.log.count()
        invalid_max_number = "invalid_integer"
        response = client.post(
                        url_for('form_bp.set_expiry_field_condition', form_id=form.id),
                        data={
                            "field_name": number_field_id,
                            "condition": invalid_max_number
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.json['condition'] == 0
        assert response.json['expired'] is False
        assert form.has_expired() is False
        assert form.can_expire() is False
        assert form.expiry_conditions['fields'] == {}
        assert form.log.count() == initial_log_count + 1

    #def test_set_expiration_settings(self, client, expiry_conditions):
    #    """ Set up expiration conditions for submit tests to be made later
    #        No assertions are made in this function. (previously tested)
    #        This is the last function in this module.
    #    """
    #    form_id = self.properties['form'].id
    #    number_field_id = "number-1620224716308"
    #    valid_max_answers = expiry_conditions['max_answers']
    #    login(editor_client, user_creds['editor'])
    #    response = editor_client.post(
    #                    f"/form/{form_id}/expiration/set-total-answers",
    #                    data = {
    #                        "total_answers": valid_max_answers,
    #                    },
    #                    follow_redirects=False,
    #                )
    #    valid_max_number = expiry_conditions['number_field_max']
    #    response = editor_client.post(
    #                    f"/form/{form_id}/expiration/set-field-condition",
    #                    data = {
    #                        "field_name": number_field_id,
    #                        "condition": valid_max_number
    #                    },
    #                    follow_redirects=False,
    #                )
