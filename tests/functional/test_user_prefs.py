"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pytest
from flask import url_for
from tests.factories import UserFactory
import flask_login
from tests import VALID_PASSWORD
from tests.utils import login, logout


class TestUserPreferences():

    @classmethod
    def setup_class(cls):
        """Create user."""
        cls.user = UserFactory(validated_email=True)
        cls.user.uploads_enabled = True
        cls.user.save()

    def test_auth(self, client):
        """Test user_bp.user_settings
                user_bp.reset_password
                user_bp.toggle_new_answer_notification_default"""
        logout(client)
        response = client.get(
                        url_for('user_bp.user_settings'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- user_login_page -->' in response.data.decode()
        response = client.get(
                        url_for('user_bp.reset_password'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- login_to_continue -->' in response.data.decode()
        response = client.post(
                        url_for('user_bp.toggle_new_answer_notification_default'),
                        follow_redirects=True,
                    )
        assert response.status_code == 401

        login(client, {'username': self.user.username, 'password': VALID_PASSWORD})
        response = client.get(
                        url_for('user_bp.reset_password'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- reset_password_page -->' in response.data.decode()

    def test_change_password(self, client):
        """Tests invalid and valid password."""

        initial_password_hash = self.user.password_hash
        invalid_password = "1234"
        response = client.post(
                        url_for('user_bp.reset_password'),
                        data={
                            "password": invalid_password,
                            "password2": invalid_password
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert self.user.password_hash == initial_password_hash
        valid_password = "this is another valid password"
        response = client.post(
                        url_for('user_bp.reset_password'),
                        data={
                            "password": valid_password,
                            "password2": valid_password
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert self.user.password_hash != initial_password_hash

    @pytest.mark.skip(reason="No way of currently testing this")
    def test_change_email(self, client):
        """ Not impletmented """

    def test_toggle_new_answer_default_notification(self, client):
        """Tests toggle bool."""

        initial_default = self.user.preferences["newAnswerNotification"]
        response = client.post(
                        url_for('user_bp.toggle_new_answer_notification_default'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert self.user.preferences["newAnswerNotification"] != initial_default
        assert isinstance(self.user.preferences["newAnswerNotification"], bool)


    #def test_change_language(self, anon_client, editor_client):
    #    """ Tests unavailable language and available language
    #        as defined in ./liberaforms/config.py
    #        Tests permission
    #    """
    #    login(editor_client, user_creds['editor'])
    #    url = "/user/change-language"
    #    response = anon_client.get(
    #                    url,
    #                    follow_redirects=True,
    #                )
    #    assert response.status_code == 200
    #    html = response.data.decode()
    #    assert '<!-- site_index_page -->' in html
    #    response = editor_client.get(
    #                    url,
    #                    follow_redirects=False,
    #                )
    #    assert response.status_code == 200
    #    html = response.data.decode()
    #    assert '<!-- change_language_page -->' in html
    #    initial_language = flask_login.current_user.preferences['language']
    #    unavailable_language = 'af' # Afrikaans
    #    response = editor_client.post(
    #                    url,
    #                    data = {
    #                        "language": unavailable_language
    #                    },
    #                    follow_redirects=True,
    #                )
    #    assert response.status_code == 200
    #    assert flask_login.current_user.preferences['language'] == initial_language
    #    available_language = 'ca'
    #    response = editor_client.post(
    #                    url,
    #                    data = {
    #                        "language": available_language
    #                    },
    #                    follow_redirects=True,
    #                )
    #    assert response.status_code == 200
    #    assert flask_login.current_user.preferences['language'] == available_language
