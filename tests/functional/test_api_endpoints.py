"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import json
from flask import url_for
from liberaforms.models.formuser import FormUser
from tests.factories import FormFactory, AnswerFactory
from tests import user_creds
from tests.utils import login, logout, random_slug

class TestApiEndpoints():

    @classmethod
    def setup_class(cls):
        cls.properties = {"answer_data": {"hello": "world"}}

    def test_requirements(self, editor):
        form = FormFactory(author=editor, slug=random_slug())
        form.enabled = True
        form.save()
        FormUser(form=form, user=editor, is_editor=True).save()
        answer = AnswerFactory(form_id=form.id,
                               author_id=editor.id,
                               data=self.properties["answer_data"])
        answer.save()
        self.properties['form'] = form
        assert form.endpoint_auth is None

    def test_auth(self, client):
        """Test api_bp.form_info
                api_bp.form_answer
                form_bp.toggle_endpoint_auth"""
        form = self.properties['form']
        response = client.get(
                        url_for('api_bp.form_info', form_id=form.id),
                    )
        assert response.status_code == 401
        response = client.get(
                        url_for('api_bp.form_answers', form_id=form.id),
                    )
        assert response.status_code == 401
        logout(client)
        response = client.post(
                        url_for('form_bp.toggle_endpoint_auth', form_id=form.id),
                    )
        assert response.status_code == 401
        login(client, user_creds["admin"])
        response = client.post(
                        url_for('form_bp.toggle_endpoint_auth', form_id=form.id),
                    )
        assert response.status_code == 401

    def test_enable_endpoints(self, client):
        form = self.properties['form']
        assert form.endpoint_auth is None
        login(client, user_creds["editor"])
        response = client.post(
                        url_for('form_bp.toggle_endpoint_auth', form_id=form.id),
                    )
        assert response.status_code == 200
        assert form.endpoint_auth.enabled is True

    def test_form_endpoint(self, client):
        form = self.properties['form']
        jwt_token = form.endpoint_auth.get_authorization_header_value()
        logout(client)
        client.environ_base["HTTP_AUTHORIZATION"] = f"Bearer {jwt_token}"
        response = client.get(
                        url_for('api_bp.form_info', form_id=form.id),
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert response.json["form"]["slug"] == form.slug

    def test_answers_endpoint(self, client):
        form = self.properties['form']
        jwt_token = form.endpoint_auth.get_authorization_header_value()
        client.environ_base["HTTP_AUTHORIZATION"] = f"Bearer {jwt_token}"
        response = client.get(
                        url_for('api_bp.form_answers', form_id=form.id),
                    )
        assert response.status_code == 200
        assert response.is_json is True
        data = response.json
        assert isinstance(data["answers"], list)
        assert isinstance(data["meta"], dict)
        assert len(data["answers"]) == 1
        assert data["answers"][0]["form"] == form.id
        assert data["answers"][0]["data"] == self.properties["answer_data"]

    def test_disable_endpoints(self, client):
        form = self.properties['form']
        login(client, user_creds["editor"])
        response = client.post(
                        url_for('form_bp.toggle_endpoint_auth', form_id=form.id),
                    )
        assert response.status_code == 200
        assert form.endpoint_auth.enabled is False

    def test_disabled_form_endpoint(self, client):
        form = self.properties['form']
        jwt_token = form.endpoint_auth.get_authorization_header_value()
        logout(client)
        client.environ_base["HTTP_AUTHORIZATION"] = f"Bearer {jwt_token}"
        response = client.get(
                        url_for('api_bp.form_info', form_id=form.id),
                    )
        assert response.status_code == 410
