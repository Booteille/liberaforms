"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import json
from flask import url_for
from liberaforms.models.site import Site
from tests import user_creds
from tests.utils import login, logout


class TestSiteCustomLanguage():

    @classmethod
    def setup_class(cls):
        cls.site = Site.find()

    def test_auth(self, editor, client):
        """Test site_bp.custom_languages
                site_bp.add_custom_language
                site_bp.set_custom_language
                site_bp.remove_custom_language"""
        logout(client)
        response = client.get(
                            url_for('site_bp.custom_languages'),
                            follow_redirects=True
                        )
        assert response.status_code == 200
        assert '<!-- login_to_continue -->' in response.data.decode()
        response = client.post(
                            url_for('site_bp.add_custom_language'),
                            follow_redirects=False
                        )
        assert response.status_code == 401
        response = client.post(
                            url_for('site_bp.set_custom_language'),
                            follow_redirects=False
                        )
        assert response.status_code == 401
        response = client.post(
                            url_for('site_bp.remove_custom_language'),
                            follow_redirects=False
                        )
        assert response.status_code == 401

        login(client, user_creds['editor'])
        response = client.get(
                            url_for('site_bp.custom_languages'),
                            follow_redirects=True
                        )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()
        response = client.post(
                            url_for('site_bp.add_custom_language'),
                            follow_redirects=False
                        )
        assert response.status_code == 401
        response = client.post(
                            url_for('site_bp.set_custom_language'),
                            follow_redirects=False
                        )
        assert response.status_code == 401
        response = client.post(
                            url_for('site_bp.remove_custom_language'),
                            follow_redirects=False
                        )
        assert response.status_code == 401

        login(client, user_creds['admin'])
        response = client.get(
                            url_for('site_bp.custom_languages'),
                            follow_redirects=False
                        )
        assert response.status_code == 200
        assert '<!-- site_custom_languages_page -->' in response.data.decode()

    def test_add_language(self, client):
        """Test a valid lang_code
           Test an invalid lang_code."""
        assert "es" not in self.site.custom_languages
        response = client.post(
                            url_for('site_bp.add_custom_language'),
                            data={
                                "lang_code": "es"
                            },
                            follow_redirects=False
                        )
        assert response.status_code == 200
        assert response.is_json is True
        assert "es" in self.site.custom_languages
        initial_languages = self.site.custom_languages
        response = client.post(
                            url_for('site_bp.add_custom_language'),
                            data={
                                "lang_code": "123"  # not a valid lang_code
                            },
                            follow_redirects=False
                        )
        assert response.status_code == 406
        assert response.is_json is True
        assert self.site.custom_languages == initial_languages

    def test_set_custom_language(self, client):
        assert self.site.language != "es"
        response = client.post(
                            url_for('site_bp.set_custom_language'),
                            data={
                                "lang_code": "es"
                            },
                            follow_redirects=False
                        )
        assert response.status_code == 200
        assert response.is_json is True
        assert self.site.language == "es"

    def test_remove_custom_language(self, client):
        assert self.site.language == "es"
        assert "es" in self.site.custom_languages
        response = client.post(
                            url_for('site_bp.remove_custom_language'),
                            data={
                                "lang_code": "es"  # the default language
                            },
                            follow_redirects=False
                        )
        assert response.status_code == 406
        assert response.is_json is True
        assert self.site.language == "es"

        assert "en" in self.site.custom_languages
        response = client.post(
                            url_for('site_bp.remove_custom_language'),
                            data={
                                "lang_code": "en"
                            },
                            follow_redirects=False
                        )
        assert response.status_code == 200
        assert response.is_json is True
        assert "en" not in self.site.custom_languages
        assert len(json.loads(response.data)['custom_languages']) == len(self.site.custom_languages)
