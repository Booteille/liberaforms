"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pytest
from flask import url_for
import flask_login
from liberaforms.models.formuser import FormUser
from tests.factories import FormFactory
from tests import user_creds
from tests.utils import login, logout, random_slug

class TestFormQR():

    @classmethod
    def setup_class(cls):
        cls.properties = {}

    def test_requirements(self, editor):
        form = FormFactory(author=editor, slug=random_slug())
        form.save()
        FormUser(form=form, user=editor, is_editor=True).save()
        self.properties['form'] = form

    def test_auth(self, client):
        "Test form_bp.form_qr."
        logout(client)
        form = self.properties['form']
        response = client.get(
                        url_for('form_bp.form_qr', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- login_to_continue -->' in response.data.decode()
        logout(client)
        login(client, user_creds['admin'])
        response = client.get(
                        url_for('form_bp.form_qr', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert "<!-- my_forms_page -->" in response.data.decode()
        logout(client)
        login(client, user_creds['editor'])

    def test_qr_page(self, client):
        form = self.properties['form']
        response = client.get(
                        url_for('form_bp.form_qr', form_id=form.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert "<!-- form_qr_page -->" in response.data.decode()

    def test_qr_download(self, client):
        form = self.properties['form']
        response = client.get(
                        url_for('form_bp.form_qr', form_id=form.id, as_png=True),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.mimetype == 'image/png'
        assert 'x89PNG' in str(response.data)
