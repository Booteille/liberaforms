"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import json
import pytest
import werkzeug
from io import BytesIO
import mimetypes
from sqlalchemy.orm.attributes import flag_modified
from flask import current_app, url_for
from liberaforms.models.site import Site
from liberaforms.models.form import Form
from liberaforms.models.formuser import FormUser
from liberaforms.models.answer import AnswerAttachment
from tests.factories import FormFactory
from tests import user_creds
from tests.utils import login, logout, random_slug, get_form_structure
from tests import utils


class TestAnswerAttachment():

    @classmethod
    def setup_class(cls):
        cls.properties = {}
        cls.site = Site.find()

    def test_requirements(self, editor, client):
        """ Create a form with file field."""
        assert editor.uploads_enabled
        structure = get_form_structure(with_attachment=True)
        form = FormFactory(author=editor,
                           slug=random_slug(),
                           structure=json.loads(structure))
        form.enabled = True
        form.save()
        FormUser(form=form, user=editor, is_editor=True).save()
        self.properties['form'] = form

    def test_submit_valid_attachment(self, client):
        """Submit 10 answers with attachment."""
        logout(client)
        form = self.properties['form']
        name = "Julia"
        total_submissions = 10
        while form.answers.count() < total_submissions:
            response = client.post(
                            form.url,
                            data={
                                "text-1620232883208": name,
                                "file-1622045746136": utils.create_file_obj("valid_attachment.pdf"),
                            },
                            follow_redirects=True,
                        )
            assert response.status_code == 200
            assert "<!-- thank_you_page -->" in response.data.decode()
        answer = form.answers[0]
        assert answer.data['text-1620232883208'] == name
        attachment = AnswerAttachment.find(form_id=form.id, answer_id=answer.id)
        assert attachment.does_file_exist() is True
        assert len(os.listdir(form.get_attachment_dir())) == total_submissions
        assert attachment.file_name == "valid_attachment.pdf"

    # @pytest.mark.skip(reason="Unsure")
    def test_submit_invalid_attachment(self, client):
        form = self.properties['form']
        self.site.mimetypes["extensions"].append("txt")
        self.site.mimetypes["mimetypes"].append("text/plain")
        flag_modified(self.site, "mimetypes")
        self.site.save()
        name = "Stella"
        response = client.post(
                        form.url,
                        data={
                            "text-1620232883208": name,
                            "file-1622045746136": utils.create_file_obj("binary_content.txt"),
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert "<!-- thank_you_page -->" in response.data.decode()
        answer = form.answers[-1]
        assert answer.data['text-1620232883208'] == name
        attachment = AnswerAttachment.find(form_id=form.id,
                                           answer_id=answer.id)
        assert attachment is None

    def test_auth(self, client):
        """Test data_display_bp.delete_answer
                answers_bp.delete_all."""
        logout(client)
        form = self.properties['form']
        answer = form.answers[0]
        response = client.delete(
                    url_for('data_display_bp.delete_answer', answer_id=answer.id),
                    follow_redirects=False,
                )
        assert response.status_code == 401
        assert response.is_json is True
        response = client.get(
                        url_for('answers_bp.delete_all', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- login_to_continue -->' in response.data.decode()

        login(client, user_creds["admin"])
        response = client.delete(
                    url_for('data_display_bp.delete_answer', answer_id=answer.id),
                    follow_redirects=False,
                )
        assert response.status_code == 401
        assert response.is_json is True
        response = client.get(
                        url_for('answers_bp.delete_all', form_id=form.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- my_forms_page -->' in response.data.decode()
        login(client, user_creds["editor"])

    def test_delete_answer_and_attachment(self, client):
        form = self.properties['form']
        initial_log_count = form.log.count()
        initial_answers_count = form.answers.count()
        answer = form.answers[0]
        attachment = AnswerAttachment.find(form_id=form.id, answer_id=answer.id)

        response = client.delete(
                    url_for('data_display_bp.delete_answer', answer_id=answer.id),
                    follow_redirects=False,
                )
        assert response.status_code == 200
        assert response.is_json is True
        assert response.json['deleted'] is True
        assert form.answers.count() == initial_answers_count - 1
        assert form.log.count() == initial_log_count + 1
        assert attachment.does_file_exist() is False

    def test_delete_all_answers_and_attachment(self, client):
        form = self.properties['form']

        initial_answers_count = form.answers.count()
        initial_log_count = form.log.count()
        response = client.get(
                        url_for('answers_bp.delete_all', form_id=form.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert f'<label for="total-answers" class="visually-hidden">{initial_answers_count}</label>'  in html
        assert os.path.isdir(form.get_attachment_dir()) is True
        login(client, user_creds["editor"])
        response = client.post(
                        url_for('answers_bp.delete_all', form_id=form.id),
                        data={
                            "totalAnswers": initial_answers_count,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert 'alert alert-success' in html
        assert '<!-- list_answers_page -->' in html
        assert form.answers.count() == 0
        assert form.log.count() == initial_log_count + 1
        assert os.path.isdir(form.get_attachment_dir()) is False
