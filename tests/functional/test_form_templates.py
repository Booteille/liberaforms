"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from bs4 import BeautifulSoup
from flask import url_for
from tests.utils import login, logout
from tests import user_creds
import flask_login
from liberaforms.form_templates.form_templates import templates as form_templates
from liberaforms.utils import i18n


class TestPreviewTemplates():
    def setup_class(cls):
        assert len(form_templates) > 0
        cls.template = form_templates[0]

    def test_requirements(self, editor):
        pass

    def test_auth(self, client):
        """Test form_bp.list_templates
                form_bp.view_template"""
        logout(client)
        response = client.get(
                        url_for('form_bp.list_templates'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- login_to_continue -->' in response.data.decode()
        response = client.get(
                        url_for('form_bp.view_template', template_id=self.template["id"]),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- login_to_continue -->' in response.data.decode()

    def test_list_templates(self, client):
        login(client, user_creds['editor'])
        response = client.get(
                        url_for('form_bp.list_templates'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- list_templates_page -->' in html
        soup = BeautifulSoup(html, features="lxml")
        main_content = soup.find("main", {"id": "main-content"})
        assert len(main_content.find_all("div", class_="card")) == len(form_templates)

    def test_view_template(self, client):
        response = client.get(
                        url_for('form_bp.view_template', template_id=self.template["id"]),
                        follow_redirects=True,
                    )
        assert response.status_code == 200

    def test_create_form(self, client):
        response = client.get(
                        url_for('form_bp.create_new_form', template_id=self.template["id"]),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert "<!-- new_form_page -->" in response.data.decode()
        assert i18n.lazytext_to_serializable(self.template["name"]) in response.data.decode()
