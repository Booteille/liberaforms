"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import json
from flask import url_for
import flask_login
from liberaforms.models.answer import Answer, AnswerEdition, AnswerAttachment
from liberaforms.models.formuser import FormUser
from tests.factories import FormFactory, AnswerFactory
from tests import user_creds
from tests.utils import login, logout
from tests import utils


class TestAnswerEdition():

    @classmethod
    def setup_class(cls):
        cls.properties = {}

    def test_requirements(self, editor, client):
        structure = utils.get_form_structure(with_attachment=True)
        form = FormFactory(author=editor,
                           slug=utils.random_slug(),
                           structure=json.loads(structure))
        form.enabled = True
        form.anon_edition = True
        form.save()
        FormUser(form=form, user=editor, is_editor=True).save()
        self.properties['form'] = form

        logout(client)
        client.post(
            form.url,
            data={
                "text-1620232883208": "Julia",
                "file-1622045746136": utils.create_file_obj("valid_attachment.pdf"),
            },
            follow_redirects=True,
        )
        answer = Answer.find(author_id=editor.id, form_id=form.id)
        assert answer.public_id
        assert AnswerAttachment.find(form_id=form.id, answer_id=answer.id)
        assert form.answers.count() == 1
        assert len(os.listdir(form.get_attachment_dir())) == 1
        self.properties['answer'] = answer

    def test_auth(self, client):
        """Test data_display_bp.update_answer."""

        answer = self.properties['answer']

        logout(client)
        response = client.post(
                    url_for('data_display_bp.update_answer', answer_id=answer.id),
                    follow_redirects=False,
                )
        assert response.status_code == 401

        login(client, user_creds['admin'])
        response = client.post(
                    url_for('data_display_bp.update_answer', answer_id=answer.id),
                    follow_redirects=False,
                )
        assert response.status_code == 401

    def test_edit_answer_field(self, editor, client):
        form = self.properties['form']
        answer = self.properties['answer']
        login(client, user_creds['editor'])

        answer_cnt = form.answers.count()
        response = client.post(
                    url_for('data_display_bp.update_answer', answer_id=answer.id),
                    json={
                        "item_data": {"text-1620232883208": "Stella",
                                      "file-1622045746136": answer.data["file-1622045746136"]}
                    },
                    follow_redirects=False,
                )
        assert response.status_code == 200
        assert response.is_json is True
        assert response.json["saved"] is True
        assert response.json["data"] == answer.data
        answer_edition = AnswerEdition.find(answer_id=answer.id)
        assert answer_edition in answer.editions
        assert answer_edition.answer == answer
        assert answer_edition.user_id == editor.id
        assert answer.data["text-1620232883208"] == "Stella"
        assert answer_edition.data["text-1620232883208"] == "Julia"
        assert answer_edition.data["file-1622045746136"] == answer.data["file-1622045746136"]
        assert form.answers.count() == answer_cnt

    def test_anon_edit_answer(self, editor, client):
        form = self.properties['form']
        answer = self.properties['answer']
        answer_cnt = form.answers.count()
        
        logout(client)
        # Test edit file.
        response = client.post(
            answer.get_anon_edition_url(),
            data={
                "text-1620232883208": "Debbie",
                "file-1622045746136": utils.create_file_obj("valid_attachment.pdf"),
            },
            follow_redirects=True,
        )
        assert response.status_code == 200
        assert "<!-- thank_you_page -->" in response.data.decode()
        assert answer.data["text-1620232883208"] == "Debbie"
        assert answer.editions.count() == 2
        answer_edition = answer.editions[0]
        assert answer_edition.data["file-1622045746136"] != answer.data["file-1622045746136"]
        assert form.answers.count() == 1
        assert len(os.listdir(form.get_attachment_dir())) == 1
        assert form.answers.count() == answer_cnt

        answer_cnt = form.answers.count()
        # Test remove file.
        response = client.post(
            answer.get_anon_edition_url(),
            data={
                "text-1620232883208": "Jackie",
                "file-1622045746136": utils.create_file_obj(),
            },
            follow_redirects=True,
        )
        assert answer.data["text-1620232883208"] == "Jackie"
        assert answer.editions.count() == 3
        assert "file-1622045746136" not in answer.data.keys()
        assert len(os.listdir(form.get_attachment_dir())) == 0
        assert form.answers.count() == answer_cnt
