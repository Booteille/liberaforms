"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import pytest
from flask import url_for
from factories import UserFactory
import flask_login
from liberaforms.models.media import Media
# from liberaforms.commands.user import create as create_user
from tests import utils
from tests.utils import login, logout
from tests import user_creds


class TestUserMedia():
    """Upload and delete Media."""

    def setup_class(self):
        self.properties = {}

    def setup_method(self):
        # setup_method called for every method
        pass

    def test_auth(self, client):
        """Test media_bp.list_user_media
                media_bp.save_media
                media_bp.delete_media."""
        logout(client)
        response = client.get(
                        url_for('media_bp.list_user_media'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- login_to_continue -->' in response.data.decode()
        response = client.post(
                        url_for('media_bp.save_media'),
                        follow_redirects=True,
                    )
        assert response.status_code == 401
        response = client.post(
                        url_for('media_bp.delete_media', media_id=1),
                        follow_redirects=True,
                    )
        assert response.status_code == 401

        login(client, user_creds['editor'])
        response = client.get(
                        url_for('media_bp.list_user_media'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- list_media_page -->' in response.data.decode()

    def test_media_upload(self, client):
        """Tests media upload."""
        login(client, user_creds['editor'])
        initial_media_count = flask_login.current_user.media.count()
        response = client.post(
                        url_for('media_bp.save_media'),
                        data={
                            'media_file': utils.create_file_obj("valid_media.png"),
                            'alt_text': "valid alternative text",
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert response.json['media']['file_name'] == "valid_media.png"
        assert flask_login.current_user.media.count() == initial_media_count + 1
        media = Media.find(id=response.json['media']['id'])
        self.properties["media"] = media
        assert media.does_media_exits() is True
        assert media.does_media_exits(thumbnail=True) is True

    def test_invaild_media_upload(self, client):
        response = client.post(
                        url_for('media_bp.save_media'),
                        data={
                            'file': utils.create_file_obj("invalid_media.json"),
                            'alt_text': "valid alternative text",
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 406
        assert response.is_json is True

    def test_delete_media(self, client):
        """ Tests delete media."""
        media = self.properties["media"]
        login(client, user_creds['admin'])
        response = client.post(
                        url_for('media_bp.delete_media', media_id=media.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 404
        login(client, user_creds['editor'])
        initial_total_media = Media.query.count()
        response = client.post(
                        url_for('media_bp.delete_media', media_id=media.id),
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert initial_total_media-1 == Media.query.count()
