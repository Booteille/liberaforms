"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from flask import url_for
import flask_login
from liberaforms.models.site import Site
from liberaforms.models.formuser import FormUser
from liberaforms.models.consent import Consent
from liberaforms.models.formconsent import FormConsent
from tests.factories import ConsentFactory, FormFactory
from tests import user_creds
from tests import utils
from tests.utils import login, logout


class TestFormConsentSiteTemplate():
    """Add a site consent to a form.
       Edit the consent."""

    @classmethod
    def setup_class(cls):
        cls.properties = {}
        cls.site = Site.find()

    def test_requirements(self, editor):
        """Create a form and a site consent with shared = True."""
        form = FormFactory(author=editor, slug=utils.random_slug())
        form.save()
        FormUser(form=form, user=editor, is_editor=True).save()
        consent = ConsentFactory(site_id=self.site.id)
        consent.shared = True
        consent.save()
        self.properties["form"] = form
        self.properties["consent"] = consent


    def test_auth(self, client):
        """Test consent_bp.link_consent_to_form
                consent_bp.edit_form_data_consent."""

        form = self.properties["form"]
        consent = self.properties["consent"]

        logout(client)
        response = client.post(
                        url_for('consent_bp.link_consent_to_form',
                                form_id=form.id,
                                consent_id=consent.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 401

        response = client.get(
                        url_for('consent_bp.edit_form_data_consent',
                                form_id=form.id,
                                consent_id=consent.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- login_to_continue -->' in response.data.decode()

        login(client, user_creds["admin"])
        response = client.post(
                        url_for('consent_bp.link_consent_to_form',
                                form_id=form.id,
                                consent_id=consent.id),
                        follow_redirects=True,
                    )
        assert response.is_json is True
        assert response.status_code == 401

    def test_add_consent(self, client):
        """Test for a FormConsent relationship with the site template."""

        form = self.properties["form"]
        consent = self.properties["consent"]
        assert form.consents.count() == 0
        assert consent.site_id
        assert consent.shared
        assert not FormConsent.find(consent_id=consent.id, form_id=form.id)
        total_consents = Consent.find_all().count()

        login(client, user_creds['editor'])
        response = client.post(
                        url_for('consent_bp.link_consent_to_form',
                                form_id=form.id),
                        data={
                            "consent_id": consent.id
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert response.is_json is True
        assert response.json['consent']
        assert response.json['html']
        assert form.consents.count() == 1

        assert form.consents[0].form_id == form.id
        assert form.consents[0].consent_id == consent.id
        assert FormConsent.find(consent_id=consent.id, form_id=form.id)
        assert Consent.find_all().count() == total_consents

    def test_edit_consent(self, editor, client):
        """Test for a copy of the site template."""

        form = self.properties["form"]
        consent = self.properties["consent"]

        response = client.get(
                        url_for('consent_bp.edit_form_data_consent',
                                form_id=form.id,
                                template_id=consent.id),
                        data={
                            "consent_id": consent.id
                        },
                        follow_redirects=True,
                    )
        html = response.data.decode()
        assert '<!-- form_edit_consent_page -->' in html
        post_action = url_for('consent_bp.edit_form_data_consent',
                              form_id=form.id,
                              template_id=consent.id,
                              _external=False)
        assert f'action="{post_action}"' in html

        new_name = "was site template"
        response = client.post(
                        url_for('consent_bp.edit_form_data_consent',
                                form_id=form.id,
                                template_id=consent.id),
                        data={
                            "name": new_name,
                            "language_selector": self.site.language,
                            "language": editor.preferences["language"],
                            "label": consent.label[self.site.language],
                            "md_text": consent.text[self.site.language]["markdown"],
                            "required": consent.required,
                            "wizard": consent.wizard
                        },
                        follow_redirects=True,
                    )
        html = response.data.decode()
        assert '<!-- form_edit_consent_page -->' in html
        assert form.consents.count() == 1
        assert consent not in form.consents
        assert not FormConsent.find(consent_id=consent.id, form_id=form.id)
        assert form.consents[0].consent.id != consent.id
        copied_consent = form.consents[0].consent
        assert copied_consent.name == new_name
        post_action = url_for('consent_bp.edit_form_data_consent',
                              form_id=form.id,
                              consent_id=copied_consent.id,
                              _external=False)
        assert f'action="{post_action}"' in html



        #assert 'add-to-library' not in response.data.decode()
        #response = client.post(
        #                url_for('consent_bp.link_consent_to_form',
        #                        form_id=form.id, template_id=consent.id),
        #                data={
        #                    "name": "name",
        #                    "language": self.site.language,
        #                    "label": "label",
        #                    "md_text": "md_text",
        #                    "required": True,
        #                    "wizard": ""
        #                },
        #                follow_redirects=True,
        #            )
        #assert response.status_code == 200
        #assert '<!-- form_edit_consent_page -->' in response.data.decode()
        #assert form.is_public()
        #assert consent not in form.get_consents()
        #assert consent.site_id == self.site.id
        #assert consent.form_id is None
        #assert Consent.find_all(form_id=form.id).count() == 1
        #self.properties["consent"] = Consent.find(form_id=form.id)
        #assert self.properties["consent"] in form.get_consents()
        #assert self.properties["consent"].site_id is None
        #assert self.properties["consent"].form_id == form.id
        #assert self.properties["consent"].template_id == consent.id
