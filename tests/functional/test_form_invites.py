"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
from faker import Faker
import factory
from flask import current_app, url_for
from liberaforms.models.site import Site
from liberaforms.models.user import User
from liberaforms.models.formuser import FormUser
from liberaforms.models.invite import Invite
from tests.factories import UserFactory, FormFactory
from tests import VALID_PASSWORD, DEFAULT_ROLE, DEFAULT_LANGUAGE
from tests import user_creds
from tests import utils
from tests.utils import login, logout


class TestFormGrantInvites():
    """Create and delete a form grant invitation."""

    @classmethod
    def setup_class(cls):
        cls.properties = {}
        cls.default_language = Site.find().language
        cls.invitee_role = "guest"
        invite = Invite.find(email=user_creds['invitee']['email'])
        if invite:
            invite.delete()
        user = User.find(email=user_creds['invitee']['email'])
        if user:
            user.delete()

    def test_requirements(self, editor):
        form = FormFactory(author=editor, slug=utils.random_slug())
        form.save()
        FormUser(form=form, user=editor, is_editor=True).save()
        self.properties["form"] = form

    def test_auth(self, client):
        """Test form_bp.new_invite
                invite_bp.invite_preview
                form_bp.delete_invite."""

        form = self.properties["form"]
        logout(client)
        response = client.get(
                        url_for("form_bp.new_invite",
                                form_id=form.id,
                                email=user_creds['invitee']['email']),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- login_to_continue -->' in response.data.decode()
        response = client.get(
                        url_for("form_bp.delete_invite",
                                invite_id=12358),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- login_to_continue -->' in response.data.decode()
        response = client.post(
                        url_for("invite_bp.invite_preview"),
                        data={
                            "language": self.default_language
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 401
        assert response.is_json is True

        login(client, user_creds['editor'])
        response = client.get(
                        url_for("form_bp.new_invite",
                                form_id=form.id,
                                email=user_creds['invitee']['email']),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- form_invite_page -->' in response.data.decode()
        response = client.post(
                        url_for("invite_bp.invite_preview"),
                        data={
                            "language": self.default_language
                        },
                        follow_redirects=False,
                    )
        assert response.status_code == 200
        assert response.is_json is True

    def test_create_invite_without_LINK_variable(self, client):
        form = self.properties["form"]
        response = client.post(
                        url_for("form_bp.new_invite",
                                form_id=form.id,
                                email=user_creds['invitee']['email']),
                        data={
                            "granted_form_id": form.id,
                            "message": "Hello",
                            "email": user_creds['invitee']['email'],
                            "role": self.invitee_role,
                            "language": self.default_language
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- form_invite_page -->' in html
        assert html.count('ds-error-message') == 1
        assert Invite.find(email=user_creds['invitee']['email']) is None

    def test_create_and_delete_invite(self, client):
        form = self.properties["form"]
        response = client.post(
                        url_for("form_bp.new_invite",
                                form_id=form.id,
                                email=user_creds['invitee']['email']),
                        data={
                            "granted_form_id": form.id,
                            "message": "Hello [LINK]",
                            "email": user_creds['invitee']['email'],
                            "role": self.invitee_role,
                            "language": self.default_language
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- form_readers_page -->' in html
        if os.environ['SKIP_EMAILS'] == "False":
            assert 'Recipient address rejected:' in html
        else:
            assert 'alert-success' in html

        new_invite = Invite.find(email=user_creds['invitee']['email'])
        assert new_invite.role == self.invitee_role

        response = client.get(
                        url_for('form_bp.delete_invite',
                                invite_id=new_invite.id),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- form_readers_page -->' in response.data.decode()
        assert not Invite.find(email=user_creds['invitee']['email'])

    def test_invite_existing_user(self, client):
        form = self.properties["form"]
        response = client.post(
                        url_for("form_bp.new_invite",
                                form_id=form.id,
                                email=user_creds['editor']['email']),
                        data={
                            "granted_form_id": form.id,
                            "message": "Hello [LINK]",
                            "email": user_creds['editor']['email'],
                            "role": self.invitee_role,
                            "language": self.default_language
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- form_invite_page -->' in html
        assert html.count('ds-error-message') == 1

    def test_create_invitation_1(self, client):
        form = self.properties["form"]
        response = client.post(
                        url_for("form_bp.new_invite",
                                form_id=form.id,
                                email=user_creds['invitee']['email']),
                        data={
                            "granted_form_id": form.id,
                            "message": "Hello [LINK]",
                            "email": user_creds['invitee']['email'],
                            "role": self.invitee_role,
                            "language": self.default_language
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- form_readers_page -->' in html
        assert user_creds['invitee']['email'] in html
        new_invite = Invite.find(email=user_creds['invitee']['email'])
        self.properties["invite"] = new_invite
        assert self.invitee_role == new_invite.role
        assert user_creds['invitee']['email'] == new_invite.email

    def test_add_reader_with_existing_invitation(self, client):
        form = self.properties["form"]
        total_form_users = FormUser.find_all(form_id=form.id).count()
        response = client.post(
                        url_for('form_bp.add_reader', form_id=form.id),
                        data={
                            "email": user_creds['invitee']['email'],
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        html = response.data.decode()
        assert '<!-- form_readers_page -->' in html
        assert total_form_users == FormUser.find_all(form_id=form.id).count()

    def test_create_existing_invitation(self, client):
        form = self.properties["form"]
        total_form_users = FormUser.find_all(form_id=form.id).count()
        response = client.post(
                        url_for("form_bp.new_invite",
                                form_id=form.id,
                                email=user_creds['invitee']['email']),
                        data={
                            "granted_form_id": form.id,
                            "message": "Hello [LINK]",
                            "email": user_creds['invitee']['email'],
                            "role": self.invitee_role,
                            "language": self.default_language
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- form_readers_page -->' in response.data.decode()
        total_form_users == FormUser.find_all(form_id=form.id).count()

    def test_consume_invitation_with_unidentified_email(self, client):
        assert self.properties["invite"].token["token"]
        unidentified_email = utils.get_unique_email()
        logout(client)
        response = client.post(
                        url_for('user_bp.create_new_user',
                                invite=self.properties["invite"].token["token"]),
                        data={
                            "username": utils.get_unique_username(),
                            "email": unidentified_email,
                            "password": VALID_PASSWORD,
                            "password2": VALID_PASSWORD,
                            "termsAndConditions": True,
                        },
                        follow_redirects=True,
                    )
        assert '<!-- unvalidated_user_page -->' in response.data.decode()
        user = User.find(email=unidentified_email)
        assert user.validated_email is False

    def test_create_invitation_2(self, client):
        form = self.properties["form"]
        login(client, user_creds['editor'])
        response = client.post(
                        url_for("form_bp.new_invite",
                                form_id=form.id,
                                email=user_creds['invitee']['email']),
                        data={
                            "granted_form_id": form.id,
                            "message": "Hello [LINK]",
                            "email": user_creds['invitee']['email'],
                            "role": self.invitee_role,
                            "language": self.default_language
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- form_readers_page -->' in response.data.decode()
        new_invite = Invite.find(email=user_creds['invitee']['email'])
        self.properties["invite"] = new_invite


    def test_consume_invitation_2(self, client):
        assert User.find(email=user_creds['invitee']['email']) is None
        logout(client)
        response = client.post(
                        url_for('user_bp.create_new_user',
                                invite=self.properties["invite"].token["token"]),
                        data={
                            "username": "thebigbadwolf",
                            "email": user_creds['invitee']['email'],
                            "password": VALID_PASSWORD,
                            "password2": VALID_PASSWORD,
                            "termsAndConditions": True,
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- user_settings_page -->' in response.data.decode()
        user = User.find(email=user_creds['invitee']['email'])
        assert user.validated_email is True
        assert user.role == self.invitee_role
        assert Invite.find(id=self.properties["invite"].id) is None

        # delete invitee user to continue testing
        user.delete()
        assert User.find(id=user.id) is None
