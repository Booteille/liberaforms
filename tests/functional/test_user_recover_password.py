"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from flask import url_for
from tests.factories import UserFactory
import flask_login
from liberaforms.utils import tokens
from tests import user_creds
from tests.utils import login


class TestSiteRecoverPassword():

    @classmethod
    def setup_class(cls):
        """Create user."""
        cls.user = UserFactory(validated_email=True)
        cls.user.save()

    def test_auth(self, client):
        """Test site_bp.recover_password"""

        response = client.get(
                        url_for('site_bp.recover_password'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- recover_password_page -->' in response.data.decode()

        login(client, user_creds["editor"])
        assert flask_login.current_user.is_authenticated
        response = client.get(
                        url_for('site_bp.recover_password'),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- recover_password_page -->' in response.data.decode()
        assert flask_login.current_user.is_anonymous

    def test_recover_unidentified_email(self, client):
        response = client.post(
                        url_for('site_bp.recover_password'),
                        data={
                            "email": UserFactory().email
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- site_index_page -->' in response.data.decode()

    def test_recover_with_unvalid_token(self, client):
        response = client.get(
                        url_for('user_bp.recover_password', token="not valid"),
                        follow_redirects=True,
                    )
        assert response.status_code == 404
        assert '<!-- page_not_found_404 -->' in response.data.decode()

    def test_recover_with_orphan_token(self, client):
        token = tokens.create_token()
        response = client.get(
                        url_for('user_bp.recover_password', token=token["token"]),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- site_index_page -->' in response.data.decode()

    def test_recover_email(self, client):
        assert not self.user.token
        response = client.post(
                        url_for('site_bp.recover_password'),
                        data={
                            "email": self.user.email
                        },
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- site_index_page -->' in response.data.decode()
        assert self.user.token

    def test_token(self, client):
        response = client.get(
                        url_for('user_bp.recover_password', token=self.user.token["token"]),
                        follow_redirects=True,
                    )
        assert response.status_code == 200
        assert '<!-- reset_password_page -->' in response.data.decode()
        assert not self.user.token
        assert flask_login.current_user.id == self.user.id
