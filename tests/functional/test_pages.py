"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

from flask import url_for
from tests import user_creds
from tests.utils import login, logout


def test_site_index_page(client):
    logout(client)
    response = client.get("/")
    assert response.status_code == 200
    assert '<div id="blurb" class="marked-up">' in response.data.decode()
    assert '<!-- site_index_page -->' in response.data.decode()


def test_page_not_found(client):
    response = client.get("/wp-content/some-content")
    assert response.status_code == 404
    assert '<!-- page_not_found_404 -->' in response.data.decode()


def test_method_not_allowed(client):
    response = client.post("/")
    assert response.status_code == 405
    assert '<!-- method_not_allowed_page -->' in response.data.decode()
