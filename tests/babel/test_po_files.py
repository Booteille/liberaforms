"""
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2023 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
"""

import os
import re
import pytest
import polib


GREEN = '\033[0;32;48m'
RED = '\033[1;31;48m'
END = '\033[1;37;0m'


class TestGettextPoFiles():
    """Test for undefined variable names in .po files
       with syntax %(variable_name)s."""

    @classmethod
    def setup_class(cls):
        cls.regex = re.compile('{}(.*?){}'.format(re.escape("%("), re.escape(")s")))
        cls.properties = {"lang_dirs": []}

    def test_requirements(self, app):
        """Load variable names from messages.pot."""

        translation_path = f"{app.config['ROOT_DIR']}/liberaforms/translations/"
        pot_path = f'{translation_path}/messages.pot'

        with open(pot_path, 'r', encoding="utf-8") as pot_file:
            pot_content = pot_file.read()
        self.properties["defined_vars"] = list(set(self.regex.findall(pot_content)))
        assert len(self.properties["defined_vars"]) > 0

        for lang_dir in os.listdir(translation_path):
            if not (lang_dir.startswith('.') or lang_dir == "messages.pot"):
                self.properties["lang_dirs"].append(f"{translation_path}{lang_dir}")
        assert len(self.properties["lang_dirs"]) > 0

    def test_po_lang_files(self):
        """Iterate {lang_dir}/LC_MESSAGES/messages.po
           Fail on unrecognized variable name."""

        print()
        for lang_dir in self.properties["lang_dirs"]:
            po_dir = f"{lang_dir}/LC_MESSAGES/messages.po"
            po_file = polib.pofile(po_dir, encoding="utf-8")
            for entry in po_file.translated_entries():
                variables = list(set(self.regex.findall(entry.msgstr)))
                if variables:
                    error = [var for var in variables if var not in self.properties["defined_vars"]]
                    if error:
                        bad_po = "/".join(po_dir.split("/")[-3:])
                        pytest.exit(f"{RED}Undefined variable in {bad_po}: %({error[0]})s{END}")
            lang_code = lang_dir.split("/")[-1:][0]
            print(f"{GREEN}PASSED{END} {lang_code}")
