

{{ with_link(_("An avatar is especially useful when you $$work with a group$$ of people, making it easy to visually identify a co-editor."), "form_editors.j2.md") }}

## {{ _("Select a file") }}

{{ _("Select a PNG or JPG image file from your PC.") }}

## {{ _("Crop your image") }}

{{ _("Adjust the circle to select the part of the image you want to use.") }}

### {{ _("How to crop a new area after zooming in or zooming out") }}

{{ _("Double-click your mouse to enter crop mode.") }}

### {{ _("How to move the image after cropping an area") }}

{{ _("Double-click your mouse to enter move mode.") }}

## {{ _("Save") }}

{{ _("After saving, you may need to refresh `<F5>` your browser to see the changes.") }}

## Who sees your avatar?

+ {{ _("Other users you share forms with") }}
+ {{ _("The %(site_name)s administrators", site_name=site.name) }}

---

[{{_("Set your avatar")}}<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>]({{url_for('user_bp.set_avatar')}})
