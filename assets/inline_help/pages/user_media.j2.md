

{{_("Images that can be used with your forms.") }}

## {{ _("Disk usage") }}

{{_("Displays information about your total disk usage.") }}

{{_("Please remember that disk space is not an infinite resource.") }} {{ _("Consider deleting unused media files.")}}

## {{ _("Upload a file") }}

{{ _("Media files may not exceed %(file_size)s.", file_size=human_readable_bytes(app_config["MAX_MEDIA_SIZE"])) }}
{{ _("Small files will load more quickly.") }}

## {{_("Media files")}}

{{ _("The list of files you have uploaded.") }}

---

[{{_("My media")}}<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>]({{url_for('media_bp.list_user_media')}})
