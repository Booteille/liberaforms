

{{ _("We would love to include more languages in LiberaForms.") }}

{% set link = i18n_docs_site_url("https://docs.liberaforms.org/participate/L10n/") %}

{{with_link(_("If you want to help, please read our $$documentation about translating$$ to learn how."),
              link, external_page=True)}}
