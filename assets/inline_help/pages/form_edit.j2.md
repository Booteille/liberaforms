


{{ _("Add, remove, configure and reorder fields by dragging and dropping or by clicking on them.") }}

{{ _("See below help for sending confirmation by email.") }}

## {{ _("The form editor") }}

{{ _("The editor is divided vertically into two parts:") }}

1. {{ _("Use the 'stage' on the left to configure your form's fields by clicking on them. Change their order by dragging them") }}
2. {{ _("Field types that can be included in your form are on the right. Click on or drag/drop them into the stage") }}

## {{ _("Fields") }}

{{ _("There are different types of fields. Short texts, long texts, multiple choices, dates, etc.") }}

{{ _("Each field is displayed as a block that contains the `Name` on the left, and `Delete`, `Edit` and `Duplicate` buttons on the right.") }}

{{ _("You can reorder fields within your form by dragging and dropping them.") }}

{{ _("Fields share these common options:") }}

+ `{{ _("Required") }}`: {{ _("Make this field mandatory. An asterisk is displayed next to the `Name` of the field") }}
+ `{{ _("Name") }}`: {{ _("What are you asking for. A short text") }}
+ `{{ _("Description") }}`: {{ _("A longer optional text that better describes the information you want") }}

### {{ _("Field types") }}

+ {{ _("Short text") }}: {{ _("Use for one line of text") }}
+ {{ _("Long text") }}: {{ _("Use for longer texts. Define how many lines of text you expected the user to need (this option is only ascetic)") }}
+ {{ _("Dropdown options") }}: {{ _("A dropdown list of selectable options") }}
+ {{ _("Single choice") }}: {{ _("A group of options. Only one option can be selected") }}
+ {{ _("Multiple choice") }}: {{ _("A group of options. Multiple options can be selected") }}
+ {{ _("Date") }}: {{ _("Users may select a date") }}
+ {{ _("Number") }}: {{ _("This field only accepts numbers") }} ({{ _("more info below") }})
+ {% if site.is_uploads_enabled() %}{{ _("File upload") }}: {{ _("Allows users to attach files to the form") }} ({{ _("more info below") }}){% endif %}
+ {{ _("Section header") }}: {{ _("Use to create a separation between blocks of questions") }}
+ {{ _("Paragraph") }}: {{ _("Displays a paragraph of text to the user. Basic HTML can be included") }}

## {{ _("View changes") }}

{{ _("Use this to see your changes before saving them.") }}

## {{ _("Numeric fields") }}

+ `Min` : {{ _("The smallest accepted number") }}
+ `Max` : {{ _("The largest accepted number") }}
+ `Step` : {{ _("The interval between accepted numbers") }}

{% if site.is_uploads_enabled() %}
{% set disk_limit = current_user.get_uploads_limit(human_readable=True) %}
## {{ _("File upload") }}

{% if not current_user.uploads_enabled %}
{{ _("Contact the site administrator to enable this option for you") }}

{% elif current_user.is_storage_limit_exceeded() %}
{{ _("You have exceeded your %(storage_limit)s storage limit.", storage_limit=disk_limit) }}
{{ _("Delete form attachments or media files to enable uploads again.") }}

{% else %}
{{ _("This option is available to you when:") }}

+ {{ _("An administrator has enabled this option for you") }}
+ {{ _("You have not exceeded your %(storage_limit)s storage limit", storage_limit=disk_limit) }}

### {{ _("Valid file types") }}

{{ _("Users may attach these file types to the form:") }} {{ ', '.join(site.mimetypes["extensions"]) | upper }}

{{ _("If you wish to enable another file type, please contact the site Administrator.") }}

{% endif %}
{% endif %}

{% include './_form_confirmation.j2.md' %}
