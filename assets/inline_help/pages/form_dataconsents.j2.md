


## {{ _("Select from the list below and add") }}

{{ with_link(_("Select a statement from $$your library$$."), "user_dataconsent.j2.md") }}

{{ with_link(_("$$Administrators$$ may optionally add statements to this list for you to use."), "roles.j2.md") }}

{{ _("Note that administrators may also attach statements to your form regardless. You can see them but cannot remove them.", site_name=site.name) }}

## {{ _("Attach one or more texts to the form") }}

{{ _("You can add more than one privacy statement to your forms. These may be optional or compulsory.") }}

{{ _("For example:") }}

+ {{ _("Main purpose: Reserve attendance to an event (mandatory)") }}
+ {{ _("Secondary: Authorize the use of photos taken of you during the event (optional)") }}
+ {{ _("Secondary: Subscribe to our newsletter (optional)") }}
