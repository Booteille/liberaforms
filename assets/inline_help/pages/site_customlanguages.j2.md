

{{ _("Custom texts, such as the front page text and Privacy statements, can be translated.") }}

{{ _("You can define which languages are available for translation.") }}

## {{ _("Default language") }}

{{ _("The default text language.") }}

## {{ _("Add a language") }}

{{ _("Write the name of the language and select it.") }}

## {{ _("How it works") }}

{{ _("You are not required to translate texts to all the languages.") }}

{{ _("Texts are displayed to your users according to their personal language setting.") }}

{{ with_link(_("Texts are displayed to $$anonymous users$$ according to their browser language setting."), "glossary.j2.md") }}

{% include './_missing_translations.j2.md' %}

---
[{{ _("Configure languages") }}<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>]({{url_for("site_bp.custom_languages")}})
