

{{ _("This page displays all users on your site.") }}

## {{ _("List of users") }}

{{ with_link(_("Click on the name of a user to $$see and edit their details$$."), "admin_user.j2.md") }}

{% include './_data_display.j2.md' %}

## {{ _("Invitations") }}

{{ with_link(_("$$Send an invitation$$ to create an account on this site."), "admin_invites.j2.md") }}

---

[{{ _("List all user accounts") }}<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>]({{url_for('admin_bp.list_users')}})
