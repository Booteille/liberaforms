

{% include './_edit_data_consent.j2.md' %}

---

[{{ _("Add a new statement to the library") }}<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>]({{url_for('consent_bp.edit_user_data_consent')}})
