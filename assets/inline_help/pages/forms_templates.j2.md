

{{ _("Choose a template to get your form started. Have a look and see if they are useful to you.") }}

{{ _("After selecting a template you are asked for the new form's name.") }}

{{ _("Your new form is a copy of the template.") }}

{{ _("You can then configure and edit the form just like any other.") }}

---

[{{ _("Form templates") }}<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>]({{url_for('form_bp.list_templates')}})
