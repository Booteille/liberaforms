

{{ _("Send an invitation via email to create a user account on this site.") }}

## {{ _("New user's email") }}

{{ _("The invitation will be sent to this address.") }}

## {{ _("New user role") }}

{{ with_link(_("Learn $$more about roles$$."), "roles.j2.md") }}

## {{ _('Invitation message') }}

{{ _("Edit this text as you wish.") }}

{{ _("The message must include the word `[LINK]`. It will be converted to a valid link when the email is sent.") }}

---

[{{ _("Send an invitation") }}<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>]({{url_for('admin_bp.new_invite')}})
