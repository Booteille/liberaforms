## {{ _("What happens if a translation is missing?") }}

{{ _("When a translation to any given language cannot be found, LiberaForms will:") }}

1. {{ _("Search for and display the default") }} ({{ default_enabled_language() }})
2. {{ _("Search for and display any other translation") }}
3. {{ _("Not display anything") }}
