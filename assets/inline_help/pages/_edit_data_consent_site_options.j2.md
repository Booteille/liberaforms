

## {{ _("Attach this statement to all forms") }}

{{ _("Force this statement to be attached to all forms") }}. {{ _("It cannot be edited by the user.") }}

## {{ _("Let users copy this statement") }}

{{ _("Users may optionally copy this statement and edit it as they wish.") }}
