
{% include './_form_context_menu.j2.md' %}

{{ _("A form is made up of four parts.") }}

1. {{ _("Introduction text") }}
2. {{ _("Form fields") }}
3. {{ _("Privacy statement") }}
4. {{ _("Thank you text") }}


## {{ _("Introduction text") }}

{{ _("This text is displayed at the top of your form.") }}

{{ _("It is a good place to explain to your users what your form is about and why they should fill it out.") }}

## {{ _("Form fields") }}

{{ with_link(_("$$Add, remove and configure$$ this form's fields."), "form_edit.j2.md") }}

## {{ _("Privacy statement") }}

{{ with_link(_("If your form solicits personal data, you may add and configure required $$privacy statements$$ here."), "user_dataconsent.j2.md") }}

## {{ _("Thank you text") }}

{{ _("Users see this text just after they have submitted the form.") }}

{{ with_link(_("Note that when the '$$Send confirmation via email$$' option is enabled, this same text is sent via email."), "form_confirmation.j2.md") }}
