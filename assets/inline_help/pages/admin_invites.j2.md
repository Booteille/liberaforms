


{{ with_link(_("Invitations are sent via email to $$create a new user$$ account on this site."), "admin_invites_new.j2.md") }}

## {{ _("List of invitations") }}

{{ _("There are two types of invitations:") }}

+ {{ _("Those sent by an Administrator: You may see the message and resend the email") }}
+ {{ _("Those sent by an Editor to share a form's answers") }}

{{ _("Pending invitations are listed here and expire %(number)s days after last being sent.", number=token_lifespan) }}

---

[{{ _("List pending invitations") }}<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>]({{url_for('admin_bp.list_invites')}})
