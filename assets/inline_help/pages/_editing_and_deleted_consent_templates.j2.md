
{{ _("Keep in mind that any editions you make will been seen on the forms that have attached it.") }}

{{ _("You cannot delete a statement if it has been attached to a form.") }}
