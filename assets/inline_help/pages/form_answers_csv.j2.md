
{{ _("CSV is a common data exchange format that can be opened as a spreadsheet.") }}

## {{ _("Import options") }}

{{ _("When opening the CSV with your spreadsheet software, use these options.") }}

+ {{ _("Character set: UTF-8") }}
+ {{ _("Separator Options: Comma") }}

## {{ _("Excel users") }}

{{ _("Instead of opening the file directly, you must import it into Excel.") }} {{ _("There are many short tutorials on the Internet.") }}
