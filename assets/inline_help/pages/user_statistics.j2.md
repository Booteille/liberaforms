

{{_("The last twelve months of your activity displayed graphically.")}}

## {{_("Show and hide data")}}

{{_("Click on the labels to display the data you want to see.")}}

## {{_("Download the graph")}}

{{_("Graphs are images like any other on the Web. Right-click on them to save them on your PC.")}}

---

[{{_("My statistics")}}<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>](/user/statistics)
