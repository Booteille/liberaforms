
{% include './_form_context_menu.j2.md' %}

{{ _("The answers to each form are displayed here. You may filter, order, export and edit them.") }}

## {{ _("Export") }}

+ {{ help_page(label="CSV", file_name="form_answers_csv.j2.md") }}: {{ _("A spread sheet file for programs like Libreoffice Calc, Gnumeric or Excel") }}
+ PDF: {{ _("Downloads a PDF file") }}
+ JSON: {{ _("A data exchange format understood by computers") }}

## {{ _("Table / cards") }}

{{ _("When a form is made up of many fields it is sometimes easier to view the answers as a card. A card displays complete answers, one after another, whereas a table displays only the first few letters of each answer. Use this option to view the answers as best fits your needs.") }}

{% include './_data_display.j2.md' %}

## {{ _("Bookmark answers") }} <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-bookmark"><path d="M19 21l-7-5-7 5V5a2 2 0 0 1 2-2h10a2 2 0 0 1 2 2z"></path></svg>

{{ _("Click on the icon to bookmark answers for your convenience.") }}

{{ with_link(_("Note that other $$editors and guests$$ share these bookmarks with you."), "roles.j2.md") }}

## {{ _("Copy and edit answers") }}

{{ _("Click on an answer's value to copy or edit it.") }}

## {{ _("Graphs") }}

{{ _("This form's answers displayed as graphs") }}.

### {{ _("Chronology") }}

{{ _("A history of each moment the form was answered.") }}

### {{ _("Download graphs") }}

{{ _("Graphs are images like any other on the Web. Right-click on them to save them on your PC.") }}

## {{ _("Options") }}

{{ _("Options are displayed according to your form's configuration.") }}

{{ _("For example. If your form includes a number field, the Numeric option in enabled.") }}

### {{ _("Filters") }}

{{ _("Use this option to define filters for single and multiple choice fields.") }}

### {{ _("Numeric") }}

{{ _("Dislpays the sum of each number field.") }}

### {{ _("Include delete fields") }}

{{ _("Visualize deleted field data.") }}

### {{ _("Delete options") }}

{{ _("Delete one individual answer and/or all form answers.") }}
