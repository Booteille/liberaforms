

{{ _("The forms you have created and those shared with you by other users.") }}

{{ with_link(_("Learn how to $$create a new form$$."), "form_new.j2.md") }}


## {{ _("The list of your forms") }}

{{ with_link(_("Click on the name of the form to manage $$the form and it's answers$$."), "form.j2.md") }}

{% include './_data_display.j2.md' %}

---

[{{ _("My forms") }}<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>]({{url_for('form_bp.my_forms')}})
