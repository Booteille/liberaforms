

{{ _("Use the resources menu to share useful links with your users") }}.

## {{ _("Display the menu") }}

{{_ ("Disable the menu if you don't want to use it.") }}

{% if is_multilanguage_site %}
  {% include './_translation_option.j2.md' %}
{% endif %}

## {{ _("Add a resource") }}

{{ _("Add a new item to the menu. First the Name of the resource and then the URL.") }}

## {{ _("Menu") }}

{{ _("Edit, reorder and delete menu items.") }} {{ _("Preview and save your changes.") }}

{% if is_multilanguage_site %}
{% include './_missing_translations.j2.md' %}
{% endif %}

---

[{{ _("Configure the resource menu") }}<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>]({{url_for('site_bp.edit_resources_menu')}})
