

{{ _("Welcome to the LiberaForms user documentation.") }}

{{ _("Please find here everything you need to know to create, publish and manage your forms.") }}

{% if is_admin %}
{{ _("The documentation also includes explanations for all your Admin options.") }}
{% endif %}

## {{ _("How it works") }}

{{ _("The documentation provides contextual help for each page you view at any given moment.") }}

{% set help_icon = '<svg xmlns="http://www.w3.org/2000/svg" width="1.3em" height="1.3em" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-help-circle"><circle cx="12" cy="12" r="10"></circle><path d="M9.09 9a3 3 0 0 1 5.83 1c0 2-3 3-3 3"></path><line x1="12" y1="17" x2="12.01" y2="17"></line></svg>' %}

{{ _("If you need help with the page at hand, click on the help icon %(icon_svg)s to display the relevant documentation.", icon_svg=help_icon) }}

{{ _("You may also use the menu on the left to browse the documentation at leisure.") }}

## {{ _("Other information") }}

{% include './_help_us_translate.j2.md' %}

{% set external_link_icon = '<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-up-right"><line x1="7" y1="17" x2="17" y2="7"></line><polyline points="7 7 17 7 17 17"></polyline></svg>' %}

{% set docs_url = i18n_docs_site_url("https://docs.liberaforms.org") %}
{% set link_text = docs_url + external_link_icon %}
{% set link = "<a href='%s' target='_blank'>%s</a>" % (docs_url, link_text) %}

{{ _("For other documentation please visit %(link)s", link=link|safe) }}
