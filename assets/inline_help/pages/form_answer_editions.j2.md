

{{ _("Consult each edition made to the answer.") }}

## {{ _("Who can edit an answer?") }}

+ {{ with_link(_("Answers can be edited by $$form editors$$"), "form_editors.j2.md") }}
+ {{ with_link(_("Editions can also be made by Anonymous users when $$enabled$$"), "form_options.j2.md") }}

## {{ _("See the changes made to the answer") }}

{{ _("Select an edition by it's data and time.") }}

## {{ _("Data submitted date") }}

+ {{ _("User") }}: {{ _("The name of the user who made the changes") }}
+ {{ _("removed") }}/{{ _("added") }}: {{ _("Each edition is compared with the previous edition.") }} {{ _("Changes are highlighted") }}
