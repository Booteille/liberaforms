

{{ _("Admins can invite people to create an account by sending a link via email.") }}

{{ _("Use this option to define a text that can be reused and edited.") }}

{% if is_multilanguage_site %}
  {% include './_translation_option.j2.md' %}
{% endif %}

## {{ _("Invitation message") }}

{{ _("The message must include the word `[LINK]`. It will be converted to a valid link when the email is sent.") }}

---

[{{ _("Edit the invitation template") }}<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>]({{url_for('site_bp.invitation_template')}})
