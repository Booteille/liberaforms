


## {{ _("Filter and order fields") }}

+ {{ _("Filter") }}: {{ _("Search for text in the fields") }}
+ {{ _("Order by") }}: {{ _("Use this to order a column, or click on the column header") }}
+ {{ _("First field") }}: {{ _("Use this to configure the order of the columns") }}
