

## {{ _("A name (for your convenience)") }}

{{ _("A name to help you find this privacy statement.") }} {{ _("It is not displayed publicly.") }}

{% if show_extended_admin_options %}
  {% include './_edit_data_consent_site_options.j2.md' %}
{% endif %}

{% if show_translation_option %}
  {% include './_translation_option.j2.md' %}
{% endif %}

## {{ _("Title") }}

{{ _("The title of your privacy statement.") }}

## {{ _("Statement text") }}

{{ _("This text tells users what you will use their data for and what you will do with it.") }}

{{ with_link(_("$$The wizard$$ can help you create a privacy statement."), "wizard.j2.md") }}

## {{ _("Agreement text") }}

{{ _("The default text `%(I_agree)s` can be edited.", I_agree=_("I agree")) }}

## {{ _("Required") }} / {{ _("Optional") }}

{{ _("When this option is set to `%(Required)s`, acceptance is mandatory.", Required=_("Required")) }}<span class="ds-required" arial-label="required"></span>
