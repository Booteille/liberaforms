


{{ _("Your time zone is used to correctly display the date and time to you.") }}

## {{ _("Time zone") }}

{{ _("Search for your country, capital city or most-known city.") }}

---

[{{ _("Change my timezone") }}<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>]({{url_for('user_bp.change_timezone')}})
