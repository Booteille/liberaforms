

{{ _("Share the form's answers with other people") }}.

## {{ _("Add a person") }}

{{ _("Enter the email of the person you want to share the answers with.") }}

{{ _("If a person with that email does not already have a user account, you have the option to send them an invitation email.") }}

## {{ _("Invitation email") }}

{{ with_link(_("$$Send an email$$ that contains a URL to create the new account."), "form_invitation.j2.md") }}

{{ _("Invitations expire %(number)s days after being sent.", number=token_lifespan) }}


## {{ _("Notifications") }}

{{ _("Like Editors, Guests receive notifications via email when a form has been answered.") }}
