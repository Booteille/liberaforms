

{{ _("Delete this form and all its answers, including attachments.") }}

{{ with_link(_("Write the form's $$slug$$ here to confirm."), "glossary.j2.md") }}

{{ _("This cannot be undone!") }}

## {{ _("Why might you delete a form and its answers?") }}

{{ _("When you have finished collecting data...") }}

+ {{ _("Data protection law requires you to delete personal data you no longer use") }}
+ {{ _("Form attachments use disk space on the server") }}
