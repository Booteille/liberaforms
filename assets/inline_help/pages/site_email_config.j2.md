

{{ _("The email server configuration is basic for LiberaForms to work correctly.") }}

## {{ _("Emails are sent when:") }}

### {{ _("Users") }}

+ {{ _("Recover a forgotten password") }}
+ {{ _("Validate a new email address") }}

### {{ _("Forms") }}

+ {{ _("A form has been answered") }}
+ {{ _("An answer has been edited") }}
+ {{ _("A form has expired") }}
+ {{ _("The form has been configured to send a confirmation email") }}

### {{ _("Admin") }}

+ {{ _("A new user has registered") }}
+ {{ _("A new form has been created") }}

---
[{{ _("Configure the email server") }}<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>](/site/email/config)
