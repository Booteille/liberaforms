

{{ _("The lists of all the forms on this site.") }} {{ _("Deleted forms are not available.") }}

## {{ _("List of forms") }}

+ {{ with_link(_("Click on the name of a form to see the $$form's details$$"), "admin_form.j2.md") }}
+ {{ with_link(_("Click on the name of an author to see $$their details$$"), "admin_user.j2.md") }}

{% include './_data_display.j2.md' %}

---

[{{ _("List all forms") }}<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>]({{url_for('admin_bp.list_forms')}})
