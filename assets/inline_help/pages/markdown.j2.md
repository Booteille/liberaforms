

{{ with_link(_("Markdown is a quick, easy way to style $$HTML$$ (web page text)."), "glossary.j2.md") }}

## {{ _("The toolbar") }}

![{{ _("The toolbar") }}]({{ url_for('static', filename='images/markdown-toolbar.png') }})

{{ _("The toolbar options generate Markdown syntax for you.") }}

{{ _("Use the toolbar's 'Preview' option <i class='fa fa-eye'></i> to check your text as you go.") }}

## {{ _("The editor") }}

{{ _("It only takes 2 minutes to learn how to write Markdown efficiently.") }}

{{ _("When you know the basics, you will find you don't need to use the toolbar.") }}

<a href="https://www.markdownguide.org/basic-syntax" target="_blank" rel="noopener noreferrer">{{ _("Complete Markdown guide") }}<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-up-right"><line x1="7" y1="17" x2="17" y2="7"></line><polyline points="7 7 17 7 17 17"></polyline></svg></a>
