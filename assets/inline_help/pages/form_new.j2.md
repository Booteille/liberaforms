

{{ with_link(_("You may create a new form from scratch or $$use a template$$."), "forms_templates.j2.md") }}

## {{ _("The name of your form") }}

{{ _("Choose a name for your form.") }} {{ _("You can change it later.") }} {{ _("It is not displayed publicly.") }}

## {{ _("Form address") }}

{{ with_link(_("This is the form's public $$URL$$."), "glossary.j2.md") }}

+ {{ _("You can edit the last part of the URL") }}
+ **{{ _("The URL cannot be changed later") }}**

{{ with_link(_("You can $$configure the form$$ after it has been created."), "form.j2.md") }}

---

[{{_("Create a new form")}}<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>]({{url_for('form_bp.create_new_form')}})
