# Formularis ètics amb LiberaForms

[LiberaForms](https://liberaforms.org/ca) és una eina de programari lliure pensada i desenvolupada com a **infraestructura comunitària, lliure i ètica** que permet crear i gestionar formularis que respecten els drets digitals de les persones que en fan ús.

Amb LiberaForms pots consultar, editar i descarregar les respostes rebudes; incloure una casella per a demanar consentiment sobre la Llei de Protecció de Dades; col·laborar amb altres usuàries compartint permisos; i moltes coses més! A més, LiberaForms és cultura lliure sota llicència AGPLv3.

Usa'l, comparteix-lo i millora'l!
