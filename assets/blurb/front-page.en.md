# Ethical forms with LiberaForms

[LiberaForms](https://liberaforms.org/en) is a libre software tool developed as **community, free and ethical infrastructure**  that makes it easy to create and manage forms that respect the digital rights of the people who use it.

With LiberaForms you can browse, edit and download the answers to your forms; include a checkbox to require Data protection law consent, collaborate with other users by sharing permissions; and much more! LiberaForms is libre culture published under the AGPLv3 license.

Use it, share it, improve it!
